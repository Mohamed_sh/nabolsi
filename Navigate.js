import { Navigation } from "react-native-navigation";
import Splash from "./Splash";

export const register = () => {
  Navigation.registerComponent("home", () => Splash);
};
