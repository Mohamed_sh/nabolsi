import React, { Component } from "react";
import { View } from "react-native";
import { Navigation } from "react-native-navigation";

export default class Splash extends Component {
  componentDidMount() {
    Navigation.startSingleScreenApp({
      screen: {
        screen: "MainScreen"
      }
    });
  }

  render() {
    return <View style={{ backgroundColor: "red" }} />;
  }
}
