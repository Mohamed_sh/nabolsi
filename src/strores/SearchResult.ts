import ListViewStore from "./ListViewStore";
import {action, computed, observable} from "mobx";
import Search from "../models/Search";
import ScreenComponent from "../screens/_base/screen-component";

class SearchResult extends ListViewStore {

    @observable searchResult?: Array<Search>;

    constructor(txt: string, key: string, column: string, category?: string) {
        super();
        this.listViewManger
            .setURL('getAdvanceSearch2.php')
            .addParam("txt", txt)
            .addParam("key", key)
            .addParam("column", column)
            .addParam("category", category)
            .setRequestMethod("POST")
            .setPageNumber(1)
            .setPageCount(30)
    }

    @action private setSearch = (data: Array<Search>) => {
        if (data.length !== 0) {
            this.searchResult = data
            this.showEmptyListComponent();
            console.log(data);
        }
        else {
            this.showEmptyListComponent();
        }
    };

    @action private addSearch = (data: Array<Search>) => {
        this.searchResult!.push(...data)
    };

    @computed
    public get getSearch(): Array<Search> {
        return this.searchResult!
    };

    @action public firstPageRequest = () => {
        ScreenComponent.showActivityIndicator();
        this.listViewManger!.firstPage()
            .then(response => { console.log("res --->>", response);
             this.handelSuccessFirstPageRequest(response)})
            .catch(error => this.handelFailedFirstPageRequest(error))
    };
    private handelSuccessFirstPageRequest = (response) => {
        this.setSearch(response.data.newsList.length !== 0 ? response.data.newsList : [])
    };
    private handelFailedFirstPageRequest = (error) => {
        console.log(error.request);
        ScreenComponent.hideActivityIndicator();
        this.setSearch([])
    };

    @action public loadMoreRequest = () => {
        this.showLoadMoreActivityIndicator();
        this.listViewManger!.nextPage()
            .then(response => this.handelSuccessLoadMoreRequest(response))
            .catch(error => this.handelFailedLoadMoreRequest(error))
    };
    private handelSuccessLoadMoreRequest = (response) => {
        this.addSearch(response.data.newsList);
        this.hideLoadMoreActivityIndicator();
    };
    private handelFailedLoadMoreRequest = (error) => {
        this.hideLoadMoreActivityIndicator();
    };


    @action public pullToRefreshRequest = () => {
        this.showPullToRefreshIndicator();
        this.listViewManger!.firstPage()
            .then(response => this.handelSuccessPullToRefreshRequest(response))
            .catch(error => this.handelFailedPullToRefreshRequest(error))

    };

    private handelSuccessPullToRefreshRequest = (response) => {
        this.hidePullToRefreshIndicator();
        this.setSearch(response.data.newsList.length !== 0 ? response.data.newsList : [])
    };
    private handelFailedPullToRefreshRequest = (error) => {
        this.hidePullToRefreshIndicator();
    };


}

export default SearchResult