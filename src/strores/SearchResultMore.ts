import ListViewStore from "./ListViewStore";
import {action, computed, observable} from "mobx";
import ScreenComponent from "../screens/_base/screen-component";
import SubCategory from "../models/SubCategory";

class SearchResultMoreStore extends ListViewStore {

    @observable searchResult?: Array<SubCategory>;

    constructor(txt: string, key: string, column: string, category?: string) {
        super();
        this.listViewManger
            .setURL('getAdvanceSearch.php')
            .addParam("txt", txt)
            .addParam("key", key)
            .addParam("column", column)
            .addParam("category", category)
            .setRequestMethod("POST")
            .setPageNumber(1)
            .setPageCount(30)
    }

    @action private setSubCategory = (data: Array<SubCategory>) => {
        if (data.length !== 0) {
            this.searchResult = data
        }
        else {
            this.showEmptyListComponent()
        }
    };

    @action private addSubCategory = (data: Array<SubCategory>) => {
        this.searchResult!.push(...data)
    };

    @computed
    public get getSubCategory(): Array<SubCategory> {
        return this.searchResult!
    };

    @action public firstPageRequest = () => {
        ScreenComponent.showActivityIndicator();
        this.listViewManger!.firstPage()
            .then(response => { console.log("res", response)
             this.handelSuccessFirstPageRequest(response)})
            .catch(error => this.handelFailedFirstPageRequest(error))
    };
    private handelSuccessFirstPageRequest = (response) => {
        console.log(response);
        // ScreenComponent.hideActivityIndicator();
        this.setSubCategory(response.data.newsList.length !== 0 ? response.data.newsList : [])
    };
    private handelFailedFirstPageRequest = (error) => {
        console.log(error.request);
        ScreenComponent.hideActivityIndicator();
        this.setSubCategory([])
    };

    @action public loadMoreRequest = () => {
        this.showLoadMoreActivityIndicator();
        this.listViewManger!.nextPage()
            .then(response => this.handelSuccessLoadMoreRequest(response))
            .catch(error => this.handelFailedLoadMoreRequest(error))
    };
    private handelSuccessLoadMoreRequest = (response) => {
        this.addSubCategory(response.data.newsList);
        this.hideLoadMoreActivityIndicator();
    };
    private handelFailedLoadMoreRequest = (error) => {
        this.hideLoadMoreActivityIndicator();
    };


    @action public pullToRefreshRequest = () => {
        this.showPullToRefreshIndicator();
        this.listViewManger!.firstPage()
            .then(response => this.handelSuccessPullToRefreshRequest(response))
            .catch(error => this.handelFailedPullToRefreshRequest(error))

    };

    private handelSuccessPullToRefreshRequest = (response) => {
        this.hidePullToRefreshIndicator();
        this.setSubCategory(response.data.newsList.length !== 0 ? response.data.newsList : [])
    };
    private handelFailedPullToRefreshRequest = (error) => {
        this.hidePullToRefreshIndicator();
    };


}

export default SearchResultMoreStore