import {action, computed, observable} from "mobx";
import Category from "../models/Category";
import * as API from "../api";
import ScreenComponent from "../screens/_base/screen-component";
import ListViewStore from "./ListViewStore";

class CategoryStore extends ListViewStore{

    @observable category?: Array<Category>;

    @action private setCategory = (data: Array<Category>) => {
        if (data.length !== 0) {
            this.category = data
        }
        else {
            this.showEmptyListComponent()
        }
    };

    @computed public get getCategory (): Array<Category> {
        return this.category!
    };

    @action public getSubCategoryPageRequest = (categoryID: number) => {
        ScreenComponent.showActivityIndicator();
        return API.Controllers.Categories.get(categoryID)
    };

    @action public firstPageRequest = (categoryID: number) => {
        ScreenComponent.showActivityIndicator();
        API.Controllers.Categories.get(categoryID)
            .then(response => this.handelSuccessFirstPageRequest(response))
            .catch(error => this.handelFailedFirstPageRequest(error))
    };
    private handelSuccessFirstPageRequest = (response) => {
        ScreenComponent.hideActivityIndicator();
        this.setCategory(response.data.categoryList)
    };
    private handelFailedFirstPageRequest = (error) => {
        ScreenComponent.hideActivityIndicator()
        this.setCategory([])
    };


    @action public pullToRefreshRequest = (categoryID: number) => {
        this.showPullToRefreshIndicator();
        API.Controllers.Categories.get(categoryID)
            .then(response => this.handelSuccessPullToRefreshRequest(response))
            .catch(error => this.handelFailedPullToRefreshRequest(error))

    };

    private handelSuccessPullToRefreshRequest = (response) => {
        this.hidePullToRefreshIndicator();
        this.setCategory(response.data.categoryList)
    };
    private handelFailedPullToRefreshRequest = (error) => {
        this.hidePullToRefreshIndicator();
    };



}

export default CategoryStore