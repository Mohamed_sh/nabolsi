import { action, computed, observable } from "mobx";
import Category from "../models/Category";
import * as API from "../api";
import ScreenComponent from "../screens/_base/screen-component";
import Wisdom from "../models/Wisdom";

class MainStore {

    public static share = new MainStore();

    @observable category?: Array<Category>;
    fatwaCategory?: Array<Category>;
    @observable wisdom?: Wisdom;

    @action private setCategory = (data: Array<Category>) => {
        this.category = data
    };

    @computed public get getCategory(): Array<Category> {
        return this.category!

    };

    @computed public get getWisdom(): Wisdom {
        return this.wisdom!
    };


    @action public getCategoryRequest = () => {
        ScreenComponent.showActivityIndicator();
        API.Controllers.Categories.get(0)
            .then(response => this.handelSuccessCategoryRequest(response))
            .catch(error => this.handelFailedCategoryRequest(error))
    };

    private handelSuccessCategoryRequest = (response) => {
        ScreenComponent.hideActivityIndicator();
        this.setCategory(response.data.categoryList)
    };
    private handelFailedCategoryRequest = (error) => {
        ScreenComponent.hideActivityIndicator()
    };



    @action public getWisdomRequest = () => {
        ScreenComponent.showActivityIndicator();
        API.Controllers.Wisdom.getSingle()
            .then(response => {
                console.log("get singleWisdom", response)
                this.handelSuccessWisdomRequest(response)
            })
            .catch(error => this.handelFailedWisdomRequest(error))
    };

    private handelSuccessWisdomRequest = (response) => {
        ScreenComponent.hideActivityIndicator();
        this.wisdom = response.data.wisdomList[0];
    };
    private handelFailedWisdomRequest = (error) => {
        ScreenComponent.hideActivityIndicator()
    }


}

export default MainStore