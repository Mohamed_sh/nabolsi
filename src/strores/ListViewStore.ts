import {action, computed, observable} from "mobx";
import {ListViewManager} from "../utils/listview-manager";

class ListViewStore {
    @observable public _isLoadMoreActivityIndicatorVisible: boolean = false;
    @observable public _isPullToRefreshIndicatorVisible: boolean = false;
    @observable public _isEmptyListComponentVisible: boolean = false;
    public listViewManger = new ListViewManager();


    @computed
    public get getLoadMoreActivityIndicatorStatues(): boolean {
        return this._isLoadMoreActivityIndicatorVisible
    }

    @action public showLoadMoreActivityIndicator = () => {
        this._isLoadMoreActivityIndicatorVisible = true;
    };

    @action public hideLoadMoreActivityIndicator = () => {
        this._isLoadMoreActivityIndicatorVisible = false;
    };

    @computed
    public get getPullToRefreshIndicatorStatues(): boolean {
        return this._isPullToRefreshIndicatorVisible
    }

    @action public showPullToRefreshIndicator = () => {
        this._isPullToRefreshIndicatorVisible = true;
    };

    @action public hidePullToRefreshIndicator = () => {
        this._isPullToRefreshIndicatorVisible = false;
    };

    @computed
    public get getEmptyListComponentStatues(): boolean {
        return this._isEmptyListComponentVisible
    }

    @action public showEmptyListComponent = () => {
        this._isEmptyListComponentVisible = true;
    };

    @action public hideEmptyListComponent = () => {
        this._isEmptyListComponentVisible = false;
    }


}
export default ListViewStore