import {action, computed, observable} from "mobx";
import Category from "../models/Category";
import * as API from "../api";
import ScreenComponent from "../screens/_base/screen-component";
import ListViewStore from "./ListViewStore";
import Wisdom from "../models/Wisdom";

class WisdomStore extends ListViewStore{

    @observable wisdom?: Array<Wisdom>;

    constructor() {
        super();
        this.listViewManger
            .setURL('getTopWisdomListNew.php')
            .setPageNumber(1)
            .setPageCount(30)
            .setRequestMethod("POST")
    }

    @action private setWisdom = (data: Array<Wisdom>) => {
        if (data.length !== 0) {
            this.wisdom = data
        }
        else {
            this.showEmptyListComponent()
        }
    };

    @action private addWisdom = (data: Array<Wisdom>) => {
        this.wisdom!.push(...data)
    };

    @computed public get getWisdom (): Array<Wisdom> {
        return this.wisdom!
    };

    @action public firstPageRequest = () => {
        ScreenComponent.showActivityIndicator();
        this.listViewManger!.firstPage()
            .then(response => this.handelSuccessFirstPageRequest(response))
            .catch(error => this.handelFailedFirstPageRequest(error))
    };
    private handelSuccessFirstPageRequest = (response) => {
        ScreenComponent.hideActivityIndicator();
        console.log("response -->>", response);
        
        this.setWisdom(response.data.wisdomList)
    };
    private handelFailedFirstPageRequest = (error) => {
        ScreenComponent.hideActivityIndicator()
    };

    @action public loadMoreRequest = () => {
        this.showLoadMoreActivityIndicator();
        this.listViewManger!.nextPage()
            .then(response => this.handelSuccessLoadMoreRequest(response))
            .catch(error => this.handelFailedLoadMoreRequest(error))
    };
    private handelSuccessLoadMoreRequest = (response) => {
        this.addWisdom(response.data.wisdomList);
        this.hideLoadMoreActivityIndicator();
    };
    private handelFailedLoadMoreRequest = (error) => {
        this.hideLoadMoreActivityIndicator();
    };


    @action public pullToRefreshRequest = () => {
        this.showPullToRefreshIndicator();
        this.listViewManger.firstPage()
            .then(response => this.handelSuccessPullToRefreshRequest(response))
            .catch(error => this.handelFailedPullToRefreshRequest(error))

    };

    private handelSuccessPullToRefreshRequest = (response) => {
        this.hidePullToRefreshIndicator();
        this.setWisdom(response.data.wisdomList)
    };
    private handelFailedPullToRefreshRequest = (error) => {
        this.hidePullToRefreshIndicator();
    };



}

export default WisdomStore