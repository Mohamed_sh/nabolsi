import {action, computed, observable} from "mobx";
import ScreenComponent from "../screens/_base/screen-component";
import ListViewStore from "./ListViewStore";
import SubCategory from "../models/SubCategory";

class SubCategoryStore extends ListViewStore {

    @observable subCategory?: Array<SubCategory>;

    constructor(id: number, parentID?: number) {
        super();
        parentID == 44 ?
            this.listViewManger
                .setURL('getByCatChildwithOrder.php')
                .addParam("category", id)
                .setRequestMethod("POST")
                .setPageNumber(1)
                .setPageCount(30)
            :
            this.listViewManger
                .setURL('getByCatPaging.php')
                .addParam("category", id)
                .addParam("column", 'all')
                .setRequestMethod("POST")
                .setPageNumber(1)
                .setPageCount(30)
    }

    @action private setSubCategory = (data: Array<SubCategory>) => {
        if (data.length !== 0) {
            this.subCategory = data
        }
        else {
            this.showEmptyListComponent()
        }
    };

    @action private addSubCategory = (data: Array<SubCategory>) => {
        this.subCategory!.push(...data)
    };

    @computed
    public get getSubCategory(): Array<SubCategory> {
        return this.subCategory!
    };

    @action public firstPageRequest = () => {
        ScreenComponent.showActivityIndicator();
        this.listViewManger!.firstPage()
            .then(response => { console.log("response -->>",response);
             this.handelSuccessFirstPageRequest(response)})
            .catch(error => this.handelFailedFirstPageRequest(error))
    };
    private handelSuccessFirstPageRequest = (response) => {
        ScreenComponent.hideActivityIndicator();
        this.setSubCategory(response.data.newsList.length !== 0 ? response.data.newsList : [])
    };
    private handelFailedFirstPageRequest = (error) => {
        console.log(error.response);
        ScreenComponent.hideActivityIndicator()
    };

    @action public loadMoreRequest = () => {
        this.showLoadMoreActivityIndicator();
        this.listViewManger!.nextPage()
            .then(response => this.handelSuccessLoadMoreRequest(response))
            .catch(error => this.handelFailedLoadMoreRequest(error))
    };
    private handelSuccessLoadMoreRequest = (response) => {
        this.addSubCategory(response.data.newsList);
        this.hideLoadMoreActivityIndicator();
    };
    private handelFailedLoadMoreRequest = (error) => {
        this.hideLoadMoreActivityIndicator();
    };


    @action public pullToRefreshRequest = () => {
        this.showPullToRefreshIndicator();
        this.listViewManger!.firstPage()
            .then(response => this.handelSuccessPullToRefreshRequest(response))
            .catch(error => this.handelFailedPullToRefreshRequest(error))

    };

    private handelSuccessPullToRefreshRequest = (response) => {
        this.hidePullToRefreshIndicator();
        this.setSubCategory(response.data.newsList.length !== 0 ? response.data.newsList : [])
    };
    private handelFailedPullToRefreshRequest = (error) => {
        this.hidePullToRefreshIndicator();
    };


}

export default SubCategoryStore