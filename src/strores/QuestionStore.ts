import {action, computed, observable} from "mobx";
import ScreenComponent from "../screens/_base/screen-component";
import * as API from "../api";
import ListViewStore from "./ListViewStore";
import Category from "../models/Category";
import SubCategory from "../models/SubCategory";

class QuestionStore extends ListViewStore {

    @observable questions?: Array<SubCategory> = [];
    fatwaCategory?: Array<Category>;
    @observable fatwaSubCategory?: Array<Category> = [];


    constructor() {
        super();
        this.listViewManger
            .setURL('getByCatChildwithOrder.php')
            .addParam("category", 1251)
            .setRequestMethod("POST")
            .setPageNumber(1)
            .setPageCount(30)
        // this.getFatwaCategoryRequest()
    }

    @action private addQuestion = (data: Array<SubCategory>) => {
        this.questions!.push(...data)
    };
    @action private setQuestion = (data: Array<SubCategory>) => {
        this.questions! = data
    };

    @computed
    public get getQuestion(): Array<SubCategory> {
        return this.questions!
    };

    @action private setFatwaCategory = (data: Array<Category>) => {
        this.fatwaCategory = data
    };

    @action private addFatwaSubCategory = (data: Array<Category>) => {
        this.fatwaSubCategory!.push(...data)
    };

    @computed
    public get getFatwaCategory(): Array<Category> {
        return this.fatwaCategory!

    };

    @action public firstPageRequest = () => {
        ScreenComponent.showActivityIndicator();
        this.listViewManger!.firstPage()
            .then(response => this.handelSuccessFirstPageRequest(response))
            .catch(error => this.handelFailedFirstPageRequest(error))
    };
    private handelSuccessFirstPageRequest = (response) => {
        ScreenComponent.hideActivityIndicator();
        this.setQuestion(response.data.newsList)
    };
    private handelFailedFirstPageRequest = (error) => {
        ScreenComponent.hideActivityIndicator();
        this.setQuestion([])

    };

    @action public loadMoreRequest = () => {
        this.showLoadMoreActivityIndicator();
        this.listViewManger!.nextPage()
            .then(response => this.handelSuccessLoadMoreRequest(response))
            .catch(error => this.handelFailedLoadMoreRequest(error))
    };
    private handelSuccessLoadMoreRequest = (response) => {
        console.log(response);
        console.log(response);
        console.log(response);
        console.log(response);
        this.addQuestion(response.data.newsList);
        this.hideLoadMoreActivityIndicator();
    };
    private handelFailedLoadMoreRequest = (error) => {
        this.hideLoadMoreActivityIndicator();
    };


    @action public pullToRefreshRequest = () => {
        this.showPullToRefreshIndicator();
        this.listViewManger!.firstPage()
            .then(response => this.handelSuccessPullToRefreshRequest(response))
            .catch(error => this.handelFailedPullToRefreshRequest(error))

    };

    private handelSuccessPullToRefreshRequest = (response) => {
        this.hidePullToRefreshIndicator();
        this.setQuestion(response.data.newsList)
    };
    private handelFailedPullToRefreshRequest = (error) => {
        this.hidePullToRefreshIndicator();
        this.setQuestion([])
    };

    // @action public getQuestionRequest = () => {
    //     this.fatwaSubCategory!.map((item, index) => {
    //         API.Controllers.Questions.get(item.id!)
    //             .then(response => this.handelSuccessQuestionPageRequest(response))
    //             .catch(error => this.handelFailedQuestionPageRequest(error))
    //     })
    // };
    // private handelSuccessQuestionPageRequest = (response) => {
    //     ScreenComponent.hideActivityIndicator();
    //     this.addQuestion(response.data.newsList[0]);
    // };
    // private handelFailedQuestionPageRequest = (error) => {
    //     ScreenComponent.hideActivityIndicator()
    // };


    // @action public pullToRefreshRequest = () => {
    //     this.questions = [];
    //     this.showPullToRefreshIndicator();
    //     this.fatwaSubCategory!.map((item, index) => {
    //         API.Controllers.Questions.get(item.id!)
    //             .then(response => this.handelSuccessPullToRefreshRequest(response))
    //             .catch(error => this.handelFailedPullToRefreshRequest(error))
    //     })
    //
    // };
    // private handelSuccessPullToRefreshRequest = (response) => {
    //     this.hidePullToRefreshIndicator();
    //     this.addQuestion(response.data.newsList[0]);
    // };
    // private handelFailedPullToRefreshRequest = (error) => {
    //     this.hidePullToRefreshIndicator();
    // };
    //
    // @action public getFatwaCategoryRequest = () => {
    //     ScreenComponent.showActivityIndicator();
    //     API.Controllers.Categories.get(1251)
    //         .then(response => this.handelSuccessFatwaCategoryRequest(response))
    //         .catch(error => this.handelFailedFatwaCategoryRequest(error))
    // };
    //
    // private handelSuccessFatwaCategoryRequest = (response) => {
    //     console.log(response)
    //     this.setFatwaCategory(response.data.categoryList);
    //     this.getFatwaSubCategoryRequest();
    // };
    // private handelFailedFatwaCategoryRequest = (error) => {
    //     ScreenComponent.hideActivityIndicator()
    // };

    // @action public getFatwaSubCategoryRequest = () => {
    //     // this.fatwaCategory!.map((item, index) => {
    //         API.Controllers.Categories.get(1251)
    //             .then(response => this.handelSuccessFatwaSubCategoryRequest(response))
    //             .catch(error => this.handelFailedFatwaSubCategoryRequest(error))
    //     });
    // };
    //
    // private handelSuccessFatwaSubCategoryRequest = (response, index) => {
    //     // this.addFatwaSubCategory(response.data.categoryList);
    //     // console.log(this.getQuestion);
    //     // if (index == this.fatwaCategory!.length - 1) {
    //     //     this.getQuestionRequest()
    //     // }
    // };
    // private handelFailedFatwaSubCategoryRequest = (error) => {
    //     ScreenComponent.hideActivityIndicator();
    //     this.setQuestion([])
    // };


}

export default QuestionStore