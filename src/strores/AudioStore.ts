import {action, computed, observable} from "mobx";
import {Image, ImageURISource, Platform, Slider} from "react-native";
import Video from "react-native-video";


const databaseOptions = {
    // schema: [Sounds.schema, User.schema],
    schemaVersion: 1
};

class AudioStore {

    public mediaPlayerRef: Video;
    public sliderOfTimeRef!: Slider;
    public repeatIconRef!: Image;
    public pauseIconRef!: Image;
    public randomIconRef!: Image;
    public favoriteIconRef!: Image;
    @observable private _volume: number = 0.5;
    @observable private _seekDuration: number = 0;
    @observable private _currentTime: number = 0;
    @observable private _isPausedPlayer: boolean = false;
    @observable private _isRepeat: boolean = false;
    @observable private _isRandom: boolean = false;
    @observable private _isFavorite: boolean = false;
    @observable private _soundImage?: string;
    @observable private _soundURL?: string;
    @observable private _isEnd?: boolean = false;



    @computed get getVolume(): number {
        return this._volume
    }

    @action setVolume = (volume: number) => {
        this._volume = volume
    };
    @computed get getSeekDuration(): number {
        return this._seekDuration
    }

    @action setSeekDuration = (duration: number) => {
        this._seekDuration = duration
    };


    @computed get getTime(): number {
        return this._currentTime
    }

    @action setTime = (value: number) => {
        this._currentTime = value
    };

    @computed get getRepeatStatues(): boolean {
        return this._isRepeat
    }

    @action setRepeatOn = () => {
        this._isRepeat = true
    };
    @action setRepeatOff = () => {
        this._isRepeat = false
    };

    @computed get getRandomStatues(): boolean {
        return this._isRandom
    }

    @action setEndOn = () => {
        this._isEnd = true
    };
    @action setEndOff = () => {
        this._isEnd = false
    };

    @computed get getEndStatues(): boolean {
        return this._isEnd!
    }

    @action setRandomOn = () => {
        this._isRandom = true;
    };
    @action setRandomOff = () => {
        this._isRandom = false
    };

    @computed get getFavoriteStatues(): boolean {
        return this._isFavorite
    }

    @action setFavoriteOn = () => {
        this._isFavorite = true;
    };
    @action setFavoriteOff = () => {
        this._isFavorite = false
    };

    @computed get getPauseStatues(): boolean {
        return this._isPausedPlayer
    }

    @action setPauseOff = () => {
        this._isPausedPlayer = false
    };
    @action setPauseOn = () => {
        this._isPausedPlayer = true
    };

    @computed get getSoundImage(): string {
        return this._soundImage!
    }

    @action setSoundImage = (soundImage: string) => {
        this._soundImage = soundImage
    };

    @computed get getSoundURL(): string {
        return this._soundURL!
    }

    @action setSoundURL = (soundURL: string) => {
        this._soundURL = soundURL
    };

    @action soundCloudRequest =  (trackID: string) => {
        fetch('http://api.soundcloud.com/tracks/'+trackID+'.json?client_id=95f22ed54a5c297b1c41f72d713623ef')
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setSoundImage(responseJson.artwork_url.replace('large', 'original'));
                this.setSoundURL(responseJson.stream_url);
            })
    };

}

export default AudioStore