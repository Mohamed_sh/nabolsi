/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View, StyleSheet} from 'react-native';
import ScreenComponent from "../_base/screen-component";
import Header from "../../components/general/Header";
import PatternBackground from "../../components/general/PatternBackground";
import Form from "../../components/advanced-search/Form";


interface Props {
}


interface State {

}

export default class AdvancedSearchScreen extends ScreenComponent<Props, State> {
    static screenID = "AdvancedSearchScreen";
    static screenName = "AdvancedSearchScreen";
    static push = () => ScreenComponent.navigator.push({screen: AdvancedSearchScreen.screenID});


    render() {
        return (
            <View style={styles.container}>
                <PatternBackground/>
                <Header showBackButton={true} title={"البحث"}/>
                <Form />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
