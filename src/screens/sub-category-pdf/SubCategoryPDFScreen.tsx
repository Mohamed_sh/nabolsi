/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View, StyleSheet, Linking} from 'react-native';
import ScreenComponent from "../_base/screen-component";
import Header from "../../components/general/Header";
import SubCategory from "../../models/SubCategory";
import Content from "../../components/sub-category-pdf/Content";
import SubCategorySoundScreen from "../sub-category-sound/SubCategorySoundScreen";
import SubCategoryVideoScreen from "../sub-category-video/SubCategoryVideoScreen";
import SubCategoryTextScreen from "../sub-category-text/SubCategoryTextScreen";
import RNPrint from 'react-native-print';
import Search from "../../components/general/Search";
import DrawerComponent from "../../components/general/DrawerComponent";


interface Props {
   object: () => SubCategory
}


interface State {
 
}



export default class SubCategoryPDFScreen extends ScreenComponent<Props, State> {
    static screenID = "SubCategoryPDFScreen";
    static screenName = "SubCategoryPDFScreen";
    static push = (object: SubCategory) => ScreenComponent.navigator.push({
        screen: SubCategoryPDFScreen.screenID,
        passProps:{object: () => object}
    });

    async printRemotePDF(object: any) {
        await RNPrint.print({filePath: "http://alhudagroup-tr.com/text/" + object.attach![0].pd})
    }

    onSubCategoryListViewCellClicked = (index: number, object: any) => {
        switch (index) {
            case 0: {
                SubCategoryTextScreen.push(object, true);
                break;
            }
            case 1: {
                SubCategorySoundScreen.push(object);
                break;
            }
            case 2: {
                SubCategoryVideoScreen.push(object);
                break;
            }
            case 3: {
                SubCategoryTextScreen.push(object, false);
                break;
            }
            case 4: {
                Linking.openURL("http://alhudagroup-tr.com/text/" + object.attach![0].pd);
                break;
            }
            case 5: {
                this.printRemotePDF(object);
                break;
            }
        }
    };


    render() {
        return (
            <DrawerComponent>
            <View style={styles.container}>
              <Header showBackButton={true} isGrayBackground={true} title={this.props.object().category_name!}/>
                <View style={{alignItems: 'center'}}>
                    <Search/>
                </View>
                <Content
                    onSubCategoryListViewCellClicked={this.onSubCategoryListViewCellClicked}
                    object={this.props.object()}
                />
            </View>
            </DrawerComponent>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
