/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View, StyleSheet, Platform, Slider, Image, Linking, WebView, Alert, AlertAndroid} from 'react-native';
import ScreenComponent from "../_base/screen-component";
import Header from "../../components/general/Header";
import Content from "../../components/sub-category-sound/Content";
import AudioStore from "../../strores/AudioStore";
import Video from "react-native-video";
import SubCategory from "../../models/SubCategory";
import {observer} from "mobx-react";
import AppConstant from "../../constant/Constant";
import SubCategoryVideoScreen from "../sub-category-video/SubCategoryVideoScreen";
import SubCategoryTextScreen from "../sub-category-text/SubCategoryTextScreen";
import RNPrint from 'react-native-print';
import Search from "../../components/general/Search";
import DrawerComponent from "../../components/general/DrawerComponent";
import SubCategoryListViewCell from "../../components/sub-category/SubCategoryListViewCell";

const pause_ic = require("../../assets/images/pause_ic.png");
const play_ic = require("../../assets/images/play_ic.png");


interface Props {
    object: () => SubCategory
}


interface State {

}

@observer
export default class SubCategorySoundScreen extends ScreenComponent<Props, State> {
    static screenID = "SubCategorySoundScreen";
    static screenName = "SubCategorySoundScreen";
    static push = (object: SubCategory) => ScreenComponent.navigator.push({
        screen: SubCategorySoundScreen.screenID,
        passProps: {object: () => object}
    });
    store = new AudioStore();

    componentDidMount() {
        //this.store.soundCloudRequest(this.props.object().attach![0].sc!)
    }

    setMediaPlayerRef = (mediaPlayerRef: Video) => {
        this.store.mediaPlayerRef = mediaPlayerRef;
    };
    setSliderOfTimeRef = (sliderOfTimeRef: Slider) => {
        this.store.sliderOfTimeRef = sliderOfTimeRef;
    };

    setPauseIconRef = (pauseIconRef: Image) => {
        this.store.pauseIconRef = pauseIconRef;
    };
    onTimeChange = (time: number) => {
        this.store.mediaPlayerRef.seek(time / 1000);
        this.store.sliderOfTimeRef.setNativeProps({value: time});
    };
    onMediaPlayerProgress = (data: any) => {
        this.store.setTime(data.currentTime * 1000);
        this.store.sliderOfTimeRef.setNativeProps({value: data.currentTime * 1000});
    };
    onMediaStartLoading = (data: any) => {
        ScreenComponent.showActivityIndicator();
    };
    onMediaFinishLoading = (data: any) => {
        this.store.setSeekDuration(data.duration * 1000);
        ScreenComponent.hideActivityIndicator();
    };
    onMediaFailedLoading = (error: any) => {
        ScreenComponent.showToast("حدث خطأ اثناء تشغيل الصوت");
        this.onPauseButtonClicked();
        ScreenComponent.hideActivityIndicator();
    };
    onMediaEnd = () => {
        this.store.setEndOn();
    };
    onPauseButtonClicked = () => {
        if (this.store.getPauseStatues) {
            this.store.setPauseOff();
            Platform.select({
                ios: () => this.store.pauseIconRef.setNativeProps({source: [Image.resolveAssetSource(pause_ic)]}),
                android: () => this.store.pauseIconRef.setNativeProps({src: [Image.resolveAssetSource(pause_ic)]}),
            })();

        }
        else {
            this.store.setPauseOn();
            Platform.select({
                ios: () => this.store.pauseIconRef.setNativeProps({source: [Image.resolveAssetSource(play_ic)]}),
                android: () => this.store.pauseIconRef.setNativeProps({src: [Image.resolveAssetSource(play_ic)]}),
            })();
        }
    };
    onReplaytButtonClicked = () => {
        this.store.setEndOff();
        this.store.mediaPlayerRef.seek(0);
        this.store.setTime(0);
        this.store.sliderOfTimeRef.setNativeProps({value: 0});
    };

    onNextButtonClicked = () => {
        this.store.mediaPlayerRef.seek(this.store.getTime / 1000 + 15);
    };
    onPreviousButtonClicked = () => {
        this.store.mediaPlayerRef.seek(this.store.getTime / 1000 - 15);
    };

    async printRemotePDF(object: any) {
        await RNPrint.print({filePath: "http://alhudagroup-tr.com/text/" + object.attach![0].pd})
    }

    onSubCategoryListViewCellClicked = (index: number, object: SubCategory) => {
        switch (index) {
            case 0: {
                SubCategoryTextScreen.push(object, true);
                break;
            }
            case 1: {
                SubCategorySoundScreen.push(object);
                break;
            }
            case 2: {
                SubCategoryVideoScreen.push(object);
                break;
            }
            case 3: {
                SubCategoryTextScreen.push(object, false);
                break;
            }
            case 4: {
                Linking.openURL("http://alhudagroup-tr.com/text/" + object.attach![0].pd);
                break;
            }
            case 5: {
                this.printRemotePDF(object);
                break;
            }
        }
    };

    render() {
        return (
            <DrawerComponent>
                <View style={styles.container}>
                    <Header showBackButton={true} isGrayBackground={true} title={this.props.object().category_name!}/>
                    <View style={{alignItems: 'center'}}>
                        <Search/>
                    </View>
                    
                    {
                    /*<Content
                        object={this.props.object()}
                        repeat={this.store.getRepeatStatues}
                        soundImage={this.store.getSoundImage}
                        soundURL={this.store.getSoundURL}
                        paused={this.store.getPauseStatues}
                        setMediaPlayerRef={this.setMediaPlayerRef}
                        setSliderOfTimeRef={this.setSliderOfTimeRef}
                        setPauseIconRef={this.setPauseIconRef}
                        time={this.store.getTime}
                        duration={this.store.getSeekDuration}
                        isEnd={this.store.getEndStatues}
                        onMediaPlayerProgress={this.onMediaPlayerProgress}
                        onMediaEnd={this.onMediaEnd}
                        onTimeChange={this.onTimeChange}
                        onMediaStartLoading={this.onMediaStartLoading}
                        onReplaytButtonClicked={this.onReplaytButtonClicked}
                        onMediaFinishLoading={this.onMediaFinishLoading}
                        onMediaFailedLoading={this.onMediaFailedLoading}
                        onPauseButtonClicked={this.onPauseButtonClicked}
                        onNextButtonClicked={this.onNextButtonClicked}
                        onPreviousButtonClicked={this.onPreviousButtonClicked}
                        onSubCategoryListViewCellClicked={this.onSubCategoryListViewCellClicked}
                    />*/
                    
                    }
                    <WebView source = {{uri: this.props.object().attach![0].sc!}} style = {{flex: 1}} />
                </View>
            </DrawerComponent>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: AppConstant.Color.White,
        flex: 1,
    },
});
