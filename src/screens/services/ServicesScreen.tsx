/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View, StyleSheet} from 'react-native';
import ScreenComponent from "../_base/screen-component";
import Header from "../../components/general/Header";


interface Props {
}


interface State {
 
}

export default class ServicesScreen extends ScreenComponent<Props, State> {
    static screenID = "ServicesScreen";
    static screenName = "ServicesScreen";
    static push = () => ScreenComponent.navigator.push({screen: ServicesScreen.screenID});


    render() {
        return (
            <View style={styles.container}>
                <Header title={'Services'}/>
              
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
