/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { View, StyleSheet, Text, TextStyle, ActivityIndicator, Linking } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import ScreenComponent from "../_base/screen-component";
import Header from "../../components/general/Header";
import PatternBackground from "../../components/general/PatternBackground";
import SearchResultListView from "../../components/search-result/SearchResultListView";
import SearchResultStore from "../../strores/SearchResult";
import SearchResult from "../../components/search-result/SearchResult";
import AppConstant from "../../constant/Constant";
import SubCategoryTextScreen from "../sub-category-text/SubCategoryTextScreen";
import SubCategorySoundScreen from "../sub-category-sound/SubCategorySoundScreen";
import SubCategoryVideoScreen from "../sub-category-video/SubCategoryVideoScreen";
import SubCategoryPDFScreen from "../sub-category-pdf/SubCategoryPDFScreen";
import RNPrint from 'react-native-print';
import { observer } from "mobx-react";
import DrawerComponent from "../../components/general/DrawerComponent";
import SearchResultMoreListView from "../../components/search-result-more/SearchResultMoreListView";
import SearchResultMoreStore from "../../strores/SearchResultMore";
import SearchResultMore from "../../components/search-result-more/SearchResultMore";


interface Props {
    data: any
}


interface State {

}

@observer
export default class SearchResultMoreScreen extends ScreenComponent<Props, State> {
    static screenID = "SearchResultMoreScreen";
    static screenName = "SearchResultMoreScreen";
    static push = (data: any) => ScreenComponent.navigator.push({
        screen: SearchResultMoreScreen.screenID, passProps: {
            data: data
        }
    });
    store = new SearchResultMoreStore(this.props.data.searchInputValue, this.props.data.helpWordInputValue, this.props.data.column, this.props.data.category);

    componentDidMount() {
        console.log("load more");

        this.store.firstPageRequest();
    }

    onLoadMoreFired = () => {
        this.store.loadMoreRequest();
    };

    onListPullToRefreshFired = () => {
        this.store.pullToRefreshRequest();
    };

    renderListEmptyComponent = () => {
        if (this.store.getEmptyListComponentStatues) {
            return (<Text style={styles.emptyListTitle as TextStyle}>لا توجد نتائج متوافقة مع كلمات البحث</Text>)
        }
        else {
            return <View />
        }
    };
    renderLoadMoreActivityIndicator = () => {
        if (this.store!.getLoadMoreActivityIndicatorStatues) {
            return (<ActivityIndicator size="large" color={AppConstant.Color.Black} />)
        }
        else {
            return <View />
        }
    };

    async printRemotePDF(object: any) {
        await RNPrint.print({ filePath: "http://alhudagroup-tr.com/text/" + object.attach![0].pd })
    }

    onSubCategoryListViewCellClicked = (index: number, object: any) => {
        switch (index) {
            case 0: {
                SubCategoryTextScreen.push(object, true);
                break;
            }
            case 1: {
                SubCategorySoundScreen.push(object);
                break;
            }
            case 2: {
                SubCategoryVideoScreen.push(object);
                break;
            }
            case 3: {
                SubCategoryTextScreen.push(object, false);
                break;
            }
            case 4: {
                Linking.openURL("http://alhudagroup-tr.com/text/" + object.attach![0].pd);
                break;
            }
            case 5: {
                this.printRemotePDF(object);
                break;
            }
        }
    };

    render() {
        return (
            <DrawerComponent>
                <View style={styles.container}>
                    <PatternBackground />
                    <Header showBackButton={true} title={"موسوعة النابلسي للعلوم الإسلامية"} />
                    {this.store.getSubCategory && <SearchResultMore length={this.store.getSubCategory.length} store={this.store} data={this.props.data} />}
                    <SearchResultMoreListView
                        data={this.store.getSubCategory}
                        columnSelected={this.props.data.column}
                        keyword={this.props.data.searchInputValue}
                        helpKeyword={this.props.data.helpWordInputValue}
                        onPullToRefreshFired={this.onListPullToRefreshFired}
                        loadMoreFired={this.onLoadMoreFired}
                        renderFooterComponent={this.renderLoadMoreActivityIndicator()}
                        renderEmptyComponent={this.renderListEmptyComponent()}
                        pullToRefreshIndicatorVisible={this.store.getPullToRefreshIndicatorStatues}
                        onSearchCellClicked={this.onSubCategoryListViewCellClicked}
                    />
                    {this.store.getSubCategory && ScreenComponent.hideActivityIndicator()}
                </View>
            </DrawerComponent>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: AppConstant.Color.White
    },
    emptyListTitle: {
        fontSize: 15,
        textAlign: 'center',
        fontFamily: AppConstant.Font.SansArabic,
        color: AppConstant.Color.Black,
    },
});
