/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View, StyleSheet, ScrollView, ImageBackground, Text, Image} from 'react-native';
import ScreenComponent from "../_base/screen-component";
import Header from "../../components/general/Header";
import WisdomSection from "../../components/main/WisdomSection";
import CategoryListView from "../../components/main/CategoryListView";
import MainScreen from "./MainScreen";
import PatternBackground from "../../components/general/PatternBackground";
import Search from "../../components/general/Search";
import MainStore from "../../strores/MainStore";
import {observer} from "mobx-react";
import Drawer from 'react-native-drawer'
import SideMenuScreen from "../../../src/screens/side-menu/SideMenuScreen";
import DrawerComponent from "../../components/general/DrawerComponent";
import AppConstant from "../../constant/Constant";
import {Navigation} from 'react-native-navigation';

interface Props {
    navigator: any
}


interface State {

}

@observer
export default class Splash extends ScreenComponent<Props, State> {
    static screenID = "Splash";
    static push = () => ScreenComponent.navigator.push({screen: Splash.screenID});

    _drawer: any;

    componentDidMount() {
        setTimeout(() => {
            Navigation.startSingleScreenApp({
                screen: {
                    screen: MainScreen.screenID,
                },
                drawer: {
                    right: {
                        screen: SideMenuScreen.screenID,
                    },
                    style: {
                        // leftDrawerWidth: 50, // optional, add this if you want a define left drawer width (50=percent)
                        // rightDrawerWidth: 50, // optional, add this if you want a define right drawer width (50=percent)
                    },
                    // type: 'TheSideBar',
                    // animationType: 'facebook',
                    // disableOpenGesture: true
                },
                appStyle: {
                    orientation: 'portrait'
                },
            });
        }, 200);
    }

    render() {
        return (
            <ImageBackground source = {require('../../assets/images/bg.png')} style = {{width: '100%', height: '100%'}}>
                <View style = {{flex:1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center',}}>
                    <View style = {{flexDirection: 'row'}}>
                        <Image source = {require('../../assets/images/logo.png')} />
                    </View>
                    <View style = {{flexDirection: 'row', marginTop: 50}}>
                        <View style = {{flexDirection: 'column'}}>
                            <Text style = {{fontFamily: AppConstant.Font.SansArabic, fontSize: 28, color: '#FFFFFF', textAlign: 'center'}}>تقدمة فاعل خير</Text>
                            <Text style = {{fontFamily: AppConstant.Font.SansArabic, fontSize: 28, color: '#FFFFFF', textAlign: 'center'}}>صدقة جارية عن روح والديه</Text>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});
