import React, { Component } from "react";
import { View } from "react-native";
import PDFView from "react-native-view-pdf";
import { observer } from "mobx-react";
import * as Api from "../../api";
@observer
export default class Splash extends Component {
  componentDidMount() {
    Api.Controllers.Wisdom.getSingle().then(res => console.log("res", res)
    )
  }
  render() {
    return (
      <View style={{ backgroundColor: "green", alignSelf: "stretch", flex: 1 }} />
    );
  }
}
