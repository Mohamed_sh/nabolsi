import React, {Component} from 'react'
import RNProgressHUB from 'react-native-progresshub';
import { RNToasty } from 'react-native-toasty'
import AppConstant from "../../constant/Constant";


interface Props {

}
interface State {
}

class ScreenComponent<State,Props> extends Component<State, Props> {

    public static navigator: any;
    public static drawer: any;

    public static navigatorStyle = {
        navBarHidden: true,
        navBarBackgroundColor: AppConstant.Color.White
    };
    public static pop = () => {
        ScreenComponent.navigator.pop()
    };
    public static dismissModal = () => {
        ScreenComponent.navigator.dismissModal()
    };

    public static closeDrawer = () => {
        ScreenComponent.navigator.toggleDrawer({
            side: 'right',
            to: 'close'
        });
    };

    public static toggleDrawer = () => {
        ScreenComponent.navigator.toggleDrawer({
            side: 'right',
            animated: true,
        });
    };

    public static resetToRoot = () => {
        ScreenComponent.navigator.resetTo({screen: "MainScreen"});
    };


    public static popToRoot = () => {
        ScreenComponent.navigator.popToRoot();
    };

    public static showActivityIndicator() {
        RNProgressHUB.showSpinIndeterminate();
    };

    public static hideActivityIndicator() {
        RNProgressHUB.dismiss();
    };


    public static showToast(message: string) {
        RNToasty.Normal({title: message});
    };

}

export default ScreenComponent;