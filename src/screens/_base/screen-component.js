"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var react_native_progresshub_1 = __importDefault(require("react-native-progresshub"));
var Constant_1 = __importDefault(require("../../constant/Constant"));
var react_native_toast_native_1 = __importDefault(require("react-native-toast-native"));
var ScreenComponent = /** @class */ (function (_super) {
    __extends(ScreenComponent, _super);
    function ScreenComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ScreenComponent.showActivityIndicator = function () {
        react_native_progresshub_1.default.showSpinIndeterminate();
    };
    ;
    ScreenComponent.hideActivityIndicator = function () {
        react_native_progresshub_1.default.dismiss();
    };
    ;
    ScreenComponent.showToast = function (message) {
        var style = {
            backgroundColor: "#3d3d3d",
            width: 300,
            height: 40,
            borderRadius: 15,
        };
        react_native_toast_native_1.default.show(message, react_native_toast_native_1.default.SHORT, react_native_toast_native_1.default.BOTTOM, style);
    };
    ;
    ScreenComponent.navigatorStyle = {
        navBarHidden: true,
        statusBarHidden: true,
        navBarTranslucent: true,
        navBarTransparent: true,
        navBarNoBorder: true,
        drawUnderNavBar: true,
        disabledBackGesture: false,
        topBarElevationShadowEnabled: false,
        navBarButtonColor: Constant_1.default.Color.White
    };
    // public static push = (screenID) => {
    //     ScreenComponent.navigator.push({
    //         screen: screenID
    //     })
    // };
    ScreenComponent.pop = function () {
        ScreenComponent.navigator.pop();
    };
    ScreenComponent.dismissModal = function () {
        ScreenComponent.navigator.dismissModal();
    };
    ScreenComponent.popToRoot = function () {
        ScreenComponent.navigator.popToRoot();
    };
    return ScreenComponent;
}(react_1.PureComponent));
exports.default = ScreenComponent;
