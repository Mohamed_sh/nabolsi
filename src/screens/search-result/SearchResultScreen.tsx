/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { View, StyleSheet, Text, TextStyle, ActivityIndicator, Linking } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import ScreenComponent from "../_base/screen-component";
import Header from "../../components/general/Header";
import PatternBackground from "../../components/general/PatternBackground";
import SearchResultListView from "../../components/search-result/SearchResultListView";
import SearchResultStore from "../../strores/SearchResult";
import SearchResult from "../../components/search-result/SearchResult";
import AppConstant from "../../constant/Constant";
import SubCategoryTextScreen from "../sub-category-text/SubCategoryTextScreen";
import SubCategorySoundScreen from "../sub-category-sound/SubCategorySoundScreen";
import SubCategoryVideoScreen from "../sub-category-video/SubCategoryVideoScreen";
import SubCategoryPDFScreen from "../sub-category-pdf/SubCategoryPDFScreen";
import RNPrint from 'react-native-print';
import { observer } from "mobx-react";
import DrawerComponent from "../../components/general/DrawerComponent";
import SearchResultMoreScreen from "../search-result-more/SearchResultMoreScreen";


interface Props {
    data: any
}


interface State {

}

@observer
export default class SearchResultScreen extends ScreenComponent<Props, State> {
    static screenID = "SearchResultScreen";
    static screenName = "SearchResultScreen";
    static push = (data: any) => ScreenComponent.navigator.push({
        screen: SearchResultScreen.screenID, passProps: {
            data: data
        }
    });
    store = new SearchResultStore(this.props.data.searchInputValue, this.props.data.helpWordInputValue, this.props.data.column, this.props.data.category);

    componentDidMount() {
        console.log("search screen");

        this.store.firstPageRequest();
    }

    onLoadMoreFired = () => {
        this.store.loadMoreRequest();
    };

    onListPullToRefreshFired = () => {
        this.store.pullToRefreshRequest();
    };

    renderListEmptyComponent = () => {
        if (this.store.getEmptyListComponentStatues) {
            return (<Text style={styles.emptyListTitle as TextStyle}>لا توجد نتائج متوافقة مع كلمات البحث</Text>)
        }
        else {
            return <View />
        }
    };
    renderLoadMoreActivityIndicator = () => {
        if (this.store!.getLoadMoreActivityIndicatorStatues) {
            return (<ActivityIndicator size="large" color={AppConstant.Color.Black} />)
        }
        else {
            return <View />
        }
    };

    async printRemotePDF(object: any) {
        await RNPrint.print({ filePath: "http://alhudagroup-tr.com/text/" + object.attach![0].pd })
    }

    onSearchCellClicked = (object: any) => {
        SearchResultMoreScreen.push(object)
    };

    render() {

        console.log("getSearch -->>", this.store.getSearch);


        return (
            <DrawerComponent>
                <View style={styles.container}>
                    <PatternBackground />
                    <Header showBackButton={true} title={"موسوعة النابلسي للعلوم الإسلامية"} />
                    {this.store.getSearch && <SearchResult store={this.store} data={this.props.data} />}
                    <SearchResultListView
                        data={this.store.getSearch}
                        columnSelected={this.props.data.column}
                        keyword={this.props.data.searchInputValue}
                        helpKeyword={this.props.data.helpWordInputValue}
                        onPullToRefreshFired={this.onListPullToRefreshFired}
                        // loadMoreFired={this.onLoadMoreFired}
                        renderFooterComponent={this.renderLoadMoreActivityIndicator()}
                        renderEmptyComponent={this.renderListEmptyComponent()}
                        pullToRefreshIndicatorVisible={this.store.getPullToRefreshIndicatorStatues}
                        onSearchCellClicked={this.onSearchCellClicked}
                    />
                    {this.store.getSearch && ScreenComponent.hideActivityIndicator()}
                </View>
            </DrawerComponent>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: AppConstant.Color.White
    },
    emptyListTitle: {
        fontSize: 15,
        textAlign: 'center',
        fontFamily: AppConstant.Font.SansArabic,
        color: AppConstant.Color.Black,
    },
});
