/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, FlatList, RefreshControl } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import SearchResultMoreListViewCell from "./SearchResultMoreListViewCell";
import SubCategory from "../../models/SubCategory";
import AppConstant from "../../constant/Constant";
import SubCategoryListViewCell from "../sub-category/SubCategoryListViewCell";
import Search from "../../models/Search";


interface Props {
    onSearchCellClicked: (index: number, object: any) => void
    data: Array<SubCategory>
    onPullToRefreshFired: () => void,
    loadMoreFired?: () => void,
    renderEmptyComponent: JSX.Element,
    renderFooterComponent: JSX.Element,
    pullToRefreshIndicatorVisible: boolean,
    keyword: string,
    columnSelected: string,
    helpKeyword: string,
}


export default class SearchResultMoreListView extends PureComponent<Props> {

    _keyExtractor = (item, index) => index.toString();
    renderRefreshControl = () => (
        <RefreshControl
            refreshing={this.props.pullToRefreshIndicatorVisible}
            tintColor={AppConstant.Color.Black}
            onRefresh={this.props.onPullToRefreshFired}
        />
    );
    _renderItem = ({ item, index }) => (
        <SearchResultMoreListViewCell
            keyword={this.props.keyword}
            helpKeyword={this.props.helpKeyword}
            object={item}
            index={index}
            columnSelected={this.props.columnSelected}
            onSearchCellClicked={this.props.onSearchCellClicked}
        />
    );

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.props.data}
                    onEndReachedThreshold={0.01}
                    onEndReached={this.props.loadMoreFired}
                    initialNumToRender={30}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                    showsVerticalScrollIndicator={false}
                    style={styles.list}
                    contentContainerStyle={styles.contentContainerStyle}
                    refreshControl={this.renderRefreshControl()}
                    ListEmptyComponent={this.props.renderEmptyComponent}
                />
                {this.props.renderFooterComponent}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1.1,
        marginTop: 3.7 * vh,
    },
    list: {},
    contentContainerStyle: {
        alignItems: 'center',
    },
});
