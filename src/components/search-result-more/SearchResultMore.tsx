/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import SearchResultStore from "../../strores/SearchResult";
import SearchResultMoreStore from "../../strores/SearchResultMore";


interface Props {
    data: any
    length: number
    store: SearchResultMoreStore
}


export default class SearchResultMore extends PureComponent<Props> {


    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text style={styles.searchResultTitle}>:نتائج البحث عن</Text>
                    <View style={styles.titleView}>
                        <View style={styles.helpWordView}>
                            <Text style={styles.helpWordTitle}>الكلمات المساعدة</Text>
                            <View style={styles.helpWordContentView}>
                                <Text style={styles.wordTitle}>{this.props.data.helpWordInputValue}</Text>
                            </View>
                        </View>
                        <View style={styles.searchWordView}>
                            <Text style={styles.searchWordTitle}>عبارة البحث</Text>
                            <View style={styles.searchWordContentView}>
                                <Text style={styles.wordTitle}>{this.props.data.searchInputValue}</Text>
                            </View>
                        </View>
                    </View>
                    <View>

                    </View>
                </View>
                <View>
                    <Text style={styles.searchResultTitle}>عدد النتائج الكلي
                        ({this.props.length})</Text>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 23.8 * vh,
        paddingHorizontal: 4.2 * vw,
        marginBottom: 2 * vh,
    },
    titleView: {
        marginTop: 1.7 * vh,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    searchResultTitle: {
        marginTop: 3.3 * vh,
        fontFamily: AppConstant.Font.SansArabicBold, fontSize: 18, textAlign: 'right'
    },
    helpWordView: {
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    helpWordTitle: {
        fontFamily: AppConstant.Font.SansArabic, fontSize: 14, color: AppConstant.Color.GrayMiddle
    },
    helpWordContentView: {
        borderWidth: 1,
        borderColor: AppConstant.Color.GrayMiddle,
        borderRadius: 5,
        width: 34.6 * vw,
        height: 5.9 * vh,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: AppConstant.Color.White
    },
    searchWordView: {
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    searchWordTitle: {
        fontFamily: AppConstant.Font.SansArabic, fontSize: 14, color: AppConstant.Color.GrayMiddle
    },
    searchWordContentView: {
        borderWidth: 1,
        borderColor: AppConstant.Color.GrayMiddle,
        borderRadius: 5,
        width: 34.6 * vw,
        height: 5.9 * vh,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: AppConstant.Color.White
    },
    wordTitle: {
        fontFamily: AppConstant.Font.SansArabic, fontSize: 14
    }
});
