/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, WebView } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import SubCategory from "../../models/SubCategory";
import { any } from "prop-types";
import Search from "../../models/Search";


interface Props {
    keyword: string,
    helpKeyword: string,
    columnSelected: string,
    object: SubCategory
    index: number
    onSearchCellClicked: (index: number, object: any) => void
}

export default class SearchResultMoreListViewCell extends PureComponent<Props> {

    renderTitle = () => {
        return (
            <Text numberOfLines={1} style={styles.mainTitleText}>
                {(this.props.columnSelected == 'title' || this.props.columnSelected == 'all') ? this.props.object.title!.trim().split(" ").map((item, index) => {
                    if (item == this.props.keyword) {
                        return <Text style={{ backgroundColor: '#FFDA44' }}> {item} </Text>
                    }
                    else if (item === this.props.helpKeyword) {
                        return <Text style={{ backgroundColor: '#FD82F9' }}> {item} </Text>
                    }
                    else {
                        return " " + item + " "
                    }
                })
                    :
                    this.props.object.title
                }
            </Text>
        )
    };
    renderDescription = () => {
        return (
            <Text numberOfLines={4} style={styles.desTitleText}>
                {this.props.columnSelected == 'text' || this.props.columnSelected == 'all' ? this.props.object.clear_text!.trim().split(" ").map((item, index) => {
                    if (item == this.props.keyword) {
                        return <Text style={{ backgroundColor: '#FFDA44' }}> {item} </Text>
                    }
                    else if (item === this.props.helpKeyword) {
                        return <Text style={{ backgroundColor: '#FD82F9' }}> {item} </Text>
                    }
                    else {
                        return " " + item + " "
                    }
                })
                    :
                    this.props.object.clear_text!
                }
            </Text>
        )
    };
    onLoadMoreClicked = (index: number, object: any) => {
        this.props.onSearchCellClicked(index, object)
    };


    render() {
        return (
            <Animatable.View useNativeDriver={true} animation={'fadeInUp'} >
                <TouchableOpacity onPress={() => this.onLoadMoreClicked(3, this.props.object)}
                    style={styles.container}>
                    <View style={styles.sectionTitleView}>
                        <Text style={styles.sectionTitle}>
                            نتائج البحث في قسم : {this.props.object.category_name}
                        </Text>
                    </View>
                    {<View style={styles.itemView}>
                        {this.renderTitle()}
                        {this.renderDescription()}
                    </View>
                    }
                </TouchableOpacity>
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 5,
        paddingVertical: 2 * vh,
        paddingHorizontal: 1.9 * vw,
        backgroundColor: AppConstant.Color.Gray,
        marginBottom: 3 * vh,
        width: 91.4 * vw,
    },
    sectionTitleView: {
        alignItems: 'center',
        minHeight: 4.8 * vh,
        borderBottomWidth: 1,
        borderBottomColor: AppConstant.Color.White
    },
    itemView: {
        flex: 1, borderRadius: 5,
    },
    sectionTitle: {
        textAlign: 'center',
        fontSize: 14,
        fontFamily: AppConstant.Font.SansArabicBold,
        color: AppConstant.Color.White,
    },
    mainTitleText: {
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 12,
        color: AppConstant.Color.White,
        textAlign: "center",
        marginTop: 1 * vh
    },
    desTitleText: {
        textAlign: "center",
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 2.8 * vw,
        color: AppConstant.Color.White,
        marginTop: 2 * vh
    }
});
