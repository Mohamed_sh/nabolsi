/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import SubCategory from "../../models/SubCategory";
import moment from "moment";
import PDFView from 'react-native-view-pdf';
import ScreenComponent from "../../screens/_base/screen-component";
import SubCategoryListViewCell from "../sub-category/SubCategoryListViewCell";


interface Props {
    object: SubCategory
    onSubCategoryListViewCellClicked: (index: number, object: SubCategory) => void

}


export default class Content extends PureComponent<Props> {


    render() {
        return (
            <ScrollView scrollEnabled={false} showsVerticalScrollIndicator={false} style={styles.container}>
                <SubCategoryListViewCell isShortTitle={true} object={this.props.object}
                    onSubCategoryListViewCellClicked={this.props.onSubCategoryListViewCellClicked} />
                <View style={{ alignItems: 'center' }}>
                    <View style={styles.textView}>
                        <PDFView
                            fadeInDuration={250.0}
                            style={{ flex: 1, marginTop: 2 * vh }}
                            resource={"http://alhudagroup-tr.com/text/" + this.props.object.attach![0].pd}
                            resourceType={'url'}
                            onError={() => {
                                ScreenComponent.showToast("حدث خطا في فتح الملف ")
                            }}
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 3.7 * vh,
    },
    cellView: {
        borderRadius: 9,
        width: 90.6 * vw,
        height: 6.7 * vh,
        flexDirection: 'row',
        shadowColor: "#000",
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: AppConstant.Color.Gray,
    },
    leftView: {
        justifyContent: 'center', alignItems: 'center'
    },
    rightView: {
        justifyContent: 'center', alignItems: 'center'

    },
    centerView: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: AppConstant.Color.White,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingHorizontal: 3.4 * vw
    },
    dataTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 11,
    },
    title: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 13,
    },
    subTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 10,
    },
    bookmarkView: {
        backgroundColor: AppConstant.Color.GrayLightTwo,
        justifyContent: 'center',
        alignItems: 'center',
        width: 6.9 * vw,
        height: 3.8 * vh,
        borderRadius: 3.8 * vh / 2,
        position: 'absolute',
        right: -6.9 * vw / 2,
        top: -3.8 * vh / 2
    },
    bookmarkImage: {
        width: 10.31,
        height: 12.19
    },

    textView: {
        width: 91.4 * vw,
        height: 72.8 * vh,
        // borderRadius: 10,
        // paddingVertical: 2.8 * vh,
        // paddingHorizontal: 2.4 * vw,
        // marginTop: 2.3 * vh,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        //
        // elevation: 5,

    },
    scrollView: {
        width: "100%"
    },
    scrollViewContent: {
        alignItems: 'flex-end'
    },
    image: {
        width: 90.6 * vw, height: 20 * vh, marginTop: 3 * vh
    }
});
