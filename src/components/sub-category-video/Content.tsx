/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import Orientation from 'react-native-orientation';
import WebView from 'react-native-android-fullscreen-webview-video';
// import Video, { ScrollView, Container } from 'react-native-af-video-player'
import { Col, Row, Grid } from "react-native-easy-grid";
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import SubCategory from "../../models/SubCategory";
import SubCategoryListViewCell from "../sub-category/SubCategoryListViewCell";
import ScreenComponent from "../../screens/_base/screen-component";


interface Props {
    object: SubCategory
    onSubCategoryListViewCellClicked: (index: number, object: SubCategory) => void
    fullScreen: () => void
    exitFullScreen: () => void

}


export default class Content extends PureComponent<Props> {

    // video;
    // scrollView;

    // componentWillUnmount() {
    //     Orientation.lockToPortrait();
    // }
    onLoadStart = () => {
        ScreenComponent.showActivityIndicator()
    };
    onLoadEnd = () => {
        ScreenComponent.hideActivityIndicator()
    };

    render() {
        return (
            <View style={styles.container}>
                {/*<Container style={styles.videoView}>*/}
                {/*<Video*/}
                {/*ref={(ref) => {this.video = ref}}*/}
                {/*rotateToFullScreen={true}*/}
                {/*playInBackground={true}*/}
                {/*onFullScreen={(value)=> {*/}
                {/*if(value){*/}
                {/*this.props.fullScreen()*/}
                {/*}*/}
                {/*else {*/}
                {/*this.props.exitFullScreen()*/}
                {/*}*/}
                {/*}}*/}
                {/*onError={e => Alert.alert(e)}*/}
                {/*url={"http://nabulsi3.com/video/" + this.props.object.attach![0].vd}*/}
                {/*/>*/}
                {/*</Container>*/}
                <WebView
                    source={{ uri: 'http://www.youtube.com/embed/' + this.props.object.attach![0].yt + '?rel=0' }}
                    onLoadEnd={this.onLoadEnd}
                    onLoadStart={this.onLoadStart}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 40 * vh,
        marginTop: 3 * vh
    },
    cellView: {
        borderRadius: 9,
        width: 90.6 * vw,
        height: 6.7 * vh,
        flexDirection: 'row',
        shadowColor: "#000",
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: AppConstant.Color.Gray,
    },
    leftView: {
        justifyContent: 'center', alignItems: 'center'
    },
    rightView: {
        justifyContent: 'center', alignItems: 'center'

    },
    centerView: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: AppConstant.Color.White,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingHorizontal: 3.4 * vw
    },
    dataTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 11,
    },
    title: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 13,
    },
    subTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 10,
    },
    videoView: {
        // width: 100 * vw,
        // height: 27.2 * vh,
        marginTop: 2.8 * vh,
    },
    bookmarkView: {
        backgroundColor: AppConstant.Color.GrayLightTwo,
        justifyContent: 'center',
        alignItems: 'center',
        width: 6.9 * vw,
        height: 3.8 * vh,
        borderRadius: 3.8 * vh / 2,
        position: 'absolute',
        right: -6.9 * vw / 2,
        top: -3.8 * vh / 2
    },
    bookmarkImage: {
        width: 10.31,
        height: 12.19
    },
    soundCoverImg: {
        width: "100%", height: 51.4 * vh
    },
    currentTimeText: {
        color: AppConstant.Color.GrayMiddle, fontFamily: AppConstant.Font.SansArabic, fontSize: 16
    },
    fullTimeText: {
        textAlign: 'right', color: AppConstant.Color.GrayMiddle, fontFamily: AppConstant.Font.SansArabic, fontSize: 16
    },
    slider: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    controlButtonView: {
        flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', paddingHorizontal: 10 * vw
    },
    prevImage: {
        width: 47, height: 47
    },
    pauseImage: {
        width: 71.6, height: 71.6
    },
    nextImage: {
        width: 47, height: 47
    }
});
