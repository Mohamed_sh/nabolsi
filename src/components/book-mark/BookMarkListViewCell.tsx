/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {PureComponent} from 'react';
import {View, StyleSheet, Image, ImageStyle, Text, TouchableOpacity, TextStyle} from 'react-native';
import {vw, vh} from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import SubCategory from "../../models/SubCategory";
import moment from "moment";


interface Props {
    object: SubCategory
    onBookMarkListViewCellClicked: (object: SubCategory) => void

}


export default class BookMarkListViewCell extends PureComponent<Props> {


    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={() => this.props.onBookMarkListViewCellClicked(this.props.object)}>
                <View style={styles.dataView}>
                    <Text style={styles.dataTitle}>
                        {moment(this.props.object.article_date).format('DD/MM/YYYY')}

                    </Text>
                </View>
                <Text numberOfLines={1} style={styles.title as TextStyle}>{this.props.object.short_title}</Text>
                <Text numberOfLines={1} style={styles.subTitle as TextStyle}>{this.props.object.title}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: AppConstant.Color.Gray,
        borderRadius: 23,
        marginBottom: 3.7 * vh,
        width: 90.6 * vw,
        minHeight: 6.7 * vh,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    },
    image: {
        width: 94,
        height: 90,
        marginLeft: 2 * vw,
        marginBottom: 1 * vh
    },
    dataView:{
        width: 23.2 * vw,
        height:'100%',
        backgroundColor: AppConstant.Color.White,
        position: 'absolute',
        borderRadius:23,
        justifyContent: 'center',
        alignItems: 'center',
        right: 0,
    },
    dataTitle:{
        color: AppConstant.Color.GreenLight,
        fontSize: 14,
        fontFamily:AppConstant.Font.SansArabic
    },
    title: {
        textAlign: 'right',
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabicBold,
        marginRight: 26.6 * vw,
        fontSize: 14,
        paddingLeft: 5 * vw,
    },
    subTitle: {
        textAlign: 'right',
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        marginRight: 26.6 * vw,
        fontSize: 12,
        paddingLeft: 5 * vw,
    }
});
