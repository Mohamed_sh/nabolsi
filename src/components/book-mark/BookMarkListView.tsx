/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {PureComponent} from 'react';
import {StyleSheet, FlatList, Text, TextStyle, View, RefreshControl} from 'react-native';
import {vw, vh} from "../../constant/UnitDim"
import BookMarkListViewCell from "./BookMarkListViewCell";
import SubCategory from "../../models/SubCategory";
import AppConstant from "../../constant/Constant";


interface Props {
    data: Array<SubCategory>,
    renderListEmptyComponent: JSX.Element
    onBookMarkListViewCellClicked: (object: SubCategory) => void
    onListPullToRefreshFired: () => void
}


export default class BookMarkListView extends PureComponent<Props> {

    _keyExtractor = (item, index) => index.toString();
    renderRefreshControl = () => (
        <RefreshControl
            refreshing={false}
            tintColor={AppConstant.Color.Black}
            onRefresh={this.props.onListPullToRefreshFired}
        />
    );
    _renderItem = ({item}) => (
        <BookMarkListViewCell
            onBookMarkListViewCellClicked={this.props.onBookMarkListViewCellClicked}
            object={item}
        />
    );



    render() {
        return (
            <FlatList
                data={this.props.data}
                renderItem={this._renderItem}
                keyExtractor={this._keyExtractor}
                showsVerticalScrollIndicator={false}
                ListEmptyComponent={this.props.renderListEmptyComponent}
                style={styles.list}
                refreshControl={this.renderRefreshControl()}
                contentContainerStyle={styles.contentContainerStyle}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    list: {
        marginTop: 3.2 * vh
    },
    contentContainerStyle: {
        alignItems: 'center'
    },

});
