/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, ScrollView, Text, TouchableOpacity } from 'react-native';
import RadioButton from 'react-native-radio-button'
import Input from "../../components/_base/input"
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import ValidationManager from "../../utils/validation-manager";


interface Props {
    onSendButtonClicked?: () => void
    validationManager?: ValidationManager
}

export default class Form extends PureComponent<Props> {

    state = {
        radioButtonOne: true,
        radioButtonTwo: false,
        radioButtonThree: false,
        radioButtonFour: false,
        radioButtonFive: false,
        radioButtonSix: false,
        radioButtonSeven: false,
        radioButtonEight: false,
        radioButtonNine: false,
        radioButtonValue: "الاستعلام عن موضوع لم أجده في الموقع",
    };
    onRadioButtonOne = () => {
        this.setState({
            radioButtonOne: true,
            radioButtonTwo: false,
            radioButtonThree: false,
            radioButtonFour: false,
            radioButtonFive: false,
            radioButtonSix: false,
            radioButtonSeven: false,
            radioButtonEight: false,
            radioButtonNine: false,
            radioButtonValue: "الاستعلام عن موضوع لم أجده في الموقع",

        })
    };
    onRadioButtonTwo = () => {
        this.setState({
            radioButtonOne: false,
            radioButtonTwo: true,
            radioButtonThree: false,
            radioButtonFour: false,
            radioButtonFive: false,
            radioButtonSix: false,
            radioButtonSeven: false,
            radioButtonEight: false,
            radioButtonNine: false,
            radioButtonValue: "اعلامنا عن صفحة لا تعمل في الموقع",

        })
    };
    onRadioButtonThree = () => {
        this.setState({
            radioButtonOne: false,
            radioButtonTwo: false,
            radioButtonThree: true,
            radioButtonFour: false,
            radioButtonFive: false,
            radioButtonSix: false,
            radioButtonSeven: false,
            radioButtonEight: false,
            radioButtonNine: false,
            radioButtonValue: "طرح فكرة جديدة لتطوير الموقع",

        })
    };
    onRadioButtonFour = () => {
        this.setState({
            radioButtonOne: false,
            radioButtonTwo: false,
            radioButtonThree: false,
            radioButtonFour: true,
            radioButtonFive: false,
            radioButtonSix: false,
            radioButtonSeven: false,
            radioButtonEight: false,
            radioButtonNine: false,
            radioButtonValue: "المشاركة في العمل والمساعدة",

        })
    };
    onRadioButtonFive = () => {
        this.setState({
            radioButtonOne: false,
            radioButtonTwo: false,
            radioButtonThree: false,
            radioButtonFour: false,
            radioButtonFive: true,
            radioButtonSix: false,
            radioButtonSeven: false,
            radioButtonEight: false,
            radioButtonNine: false,
            radioButtonValue: "موضوع خاص بالمرأة",

        })
    };

    onRadioButtonSix = () => {
        this.setState({
            radioButtonOne: false,
            radioButtonTwo: false,
            radioButtonThree: false,
            radioButtonFour: false,
            radioButtonFive: false,
            radioButtonSix: true,
            radioButtonSeven: false,
            radioButtonEight: false,
            radioButtonNine: false,
            radioButtonValue: "موضوع خاص بالشباب",

        })
    };
    onRadioButtonSeven = () => {
        this.setState({
            radioButtonOne: false,
            radioButtonTwo: false,
            radioButtonThree: false,
            radioButtonFour: false,
            radioButtonFive: false,
            radioButtonSix: false,
            radioButtonSeven: true,
            radioButtonEight: false,
            radioButtonNine: false,
            radioButtonValue: "الاتصال مع الدعم الفني",

        })
    };

    onRadioButtonEight = () => {
        this.setState({
            radioButtonOne: false,
            radioButtonTwo: false,
            radioButtonThree: false,
            radioButtonFour: false,
            radioButtonFive: false,
            radioButtonSix: false,
            radioButtonSeven: false,
            radioButtonEight: true,
            radioButtonNine: false,
            radioButtonValue: "الاتصال مع إدارة الموقع",

        })
    };
    onRadioButtonNine = () => {
        this.setState({
            radioButtonOne: false,
            radioButtonTwo: false,
            radioButtonThree: false,
            radioButtonFour: false,
            radioButtonFive: false,
            radioButtonSix: false,
            radioButtonSeven: false,
            radioButtonEight: false,
            radioButtonNine: true,
            radioButtonValue: "موضوعات أخرى",

        })
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inputView}>
                    <Text style={styles.inputTitle}>
                        الاسم
                    </Text>
                    <Input
                        itemStyle={styles.inputItem}
                        style={styles.input}
                        placeholder={'قم بادخال اسمك هنا'}
                        attributeName={'الاسم'}
                        constraints={'required'}
                        fieldName={'name'}
                        validationManager={this.props.validationManager}
                        errorMessageStyle={styles.errorMessageStyle}
                        placeholderTextColor={AppConstant.Color.GrayLightThree}
                        returnKeyType={'next'}
                    />
                </View>
                <View style={styles.inputView}>
                    <Text style={styles.inputTitle}>
                        البريد الالكتروني
                    </Text>
                    <Input
                        itemStyle={styles.inputItem}
                        style={styles.input}
                        attributeName={'الايميل'}
                        constraints={'required|email'}
                        fieldName={'email'}
                        keyboardType={'email-address'}
                        validationManager={this.props.validationManager}
                        errorMessageStyle={styles.errorMessageStyle}
                        placeholder={'قم بادخال بريدك الالكتروني هنا'}
                        placeholderTextColor={AppConstant.Color.GrayLightThree}
                        returnKeyType={'next'}
                    />
                </View>
                <View style={{ alignItems: 'flex-end' }}>
                    <Text style={[styles.inputTitle, {
                        marginBottom: 3 * vh,
                        fontFamily: AppConstant.Font.SansArabicBold
                    }]}>
                        يرجى اختيار عنوان الموضوع
                    </Text>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>الاستعلام عن موضوع لم أجده في الموقع </Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonOne}
                            onPress={this.onRadioButtonOne}
                        />
                    </View>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>اعلامنا عن صفحة لا تعمل في الموقع</Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonTwo}
                            onPress={this.onRadioButtonTwo}
                        />
                    </View>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>طرح فكرة جديدة لتطوير الموقع</Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonThree}
                            onPress={this.onRadioButtonThree}
                        />
                    </View>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>المشاركة في العمل والمساعدة</Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonFour}
                            onPress={this.onRadioButtonFour}
                        />
                    </View>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>موضوع خاص بالمرأة</Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonFive}
                            onPress={this.onRadioButtonFive}
                        />
                    </View>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>موضوع خاص بالشباب</Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonSix}
                            onPress={this.onRadioButtonSix}
                        />
                    </View>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>الاتصال مع الدعم الفني</Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonSeven}
                            onPress={this.onRadioButtonSeven}
                        />
                    </View>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>الاتصال مع إدارة الموقع </Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonEight}
                            onPress={this.onRadioButtonEight}
                        />
                    </View>
                    <View style={styles.radioButtonView}>
                        <Text style={styles.radioButtonLabel}>موضوعات أخرى</Text>
                        <RadioButton
                            size={12}
                            animation={'bounceIn'}
                            innerColor={AppConstant.Color.Gray}
                            outerColor={AppConstant.Color.Gray}
                            isSelected={this.state.radioButtonNine}
                            onPress={this.onRadioButtonNine}
                        />
                    </View>
                    <Input
                        update={true}
                        attributeName={'حالة'}
                        constraints={'required'}
                        fieldName={'subjectTitle'}
                        validationManager={this.props.validationManager}
                        errorMessageStyle={styles.errorMessageStyle}
                        style={{ width: 0, height: 0, position: 'absolute' }}
                        defaultValue={this.state.radioButtonValue}
                    />
                </View>

                <View style={styles.inputView}>
                    <Text style={styles.inputTitle}>
                        الموضوع
                    </Text>
                    <Input
                        itemStyle={styles.inputTextAreaItem}
                        style={styles.input}
                        placeholder={'قم بادخال الموضوع'}
                        attributeName={'الموضوع'}
                        constraints={'required'}
                        fieldName={'subject'}
                        validationManager={this.props.validationManager}
                        errorMessageStyle={styles.errorMessageStyle}
                        multiline={true}
                        placeholderTextColor={AppConstant.Color.GrayLightThree}
                        returnKeyType={'next'}
                    />
                </View>
                <TouchableOpacity style={styles.buttonView} onPress={this.props.onSendButtonClicked}>
                    <Text style={styles.buttonText}>
                        إرسال
                    </Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 2.5 * vh,
        paddingHorizontal: 4.2 * vw,
    },
    inputView: {
        marginBottom: 3.2 * vh
    },
    inputTitle: {
        textAlign: 'right', fontFamily: AppConstant.Font.SansArabic, fontSize: 18
    },
    inputItem: {
        width: '100%',
        marginTop: 1 * vh,
        borderWidth: 1,
        borderColor: AppConstant.Color.GrayMiddle,
        justifyContent: 'center',
        borderRadius: 5,
        backgroundColor: AppConstant.Color.White
    },
    input: {
        flex: 1,
        height: 6 * vh,
        textAlignVertical: 'top',
        textAlign: 'right',
        marginRight: 8.8 * vw,
        color: AppConstant.Color.Black,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 1.7 * vh
    },
    errorMessageStyle: {
        textAlign: 'right',
        fontSize: 10,
        marginRight: 8.8 * vw,
        fontFamily: AppConstant.Font.SansArabic,
        color: 'red',
    },
    inputTextAreaItem: {
        minHeight: 35.9 * vh,
        width: '100%',
        marginTop: 1 * vh,
        borderWidth: 1,
        borderColor: AppConstant.Color.GrayMiddle,
        borderRadius: 5,
        backgroundColor: AppConstant.Color.White
    },
    buttonView: {
        width: "100%",
        backgroundColor: AppConstant.Color.Yellow,
        height: 5.9 * vh,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonText: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabicBold,
        fontSize: 16
    },
    radioButtonLabel: {
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 16,
        color: AppConstant.Color.Gray,
        marginRight: 3 * vw
    },
    radioButtonView: {
        flexDirection: 'row',
        marginBottom: 3 * vh
    }
});
