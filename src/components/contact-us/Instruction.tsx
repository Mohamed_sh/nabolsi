/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, TextStyle } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";


interface Props {
}


export default class Instruction extends PureComponent<Props> {


    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title as TextStyle}>السلام عليكم</Text>
                <Text style={styles.description as TextStyle}>
                    انطلاقاً من قوله تعالى: (وتعاونوا على البر والتقوى) ومن إيماننا العميق بأهمية التواصل مع زوار موقعنا
                    الأكارم لتحقيق الأفضل وتطوير العمل بشكل مستمر كانت هذه الصفحة التفاعلية للتواصل مع إدارة الموقع في
                    كل ما يهم رواده أو يشغل بالهم. إن إدارة الموقع مستعدة للإجابة عن جميع استفساراتكم عن طريق نظام بريدي
                    متطور يحول رسائلكم إلى القسم المختص حيث يتم الرد على أي تساؤل أو مقترح في مدة لا تتجاوز اليومين.
                </Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 5 * vh,
        paddingHorizontal: 2.4 * vh,
    },
    title: {
        textAlign: 'right',
        fontFamily: AppConstant.Font.SansArabicBold,
        fontSize: 14,
        marginBottom: 3.5 * vh,

    },
    description: {
        textAlign: 'right',
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 12,
        marginBottom: 3.5 * vh,
    }
});
