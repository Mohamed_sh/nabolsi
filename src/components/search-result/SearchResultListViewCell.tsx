/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, WebView } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import SubCategory from "../../models/SubCategory";
import { any } from "prop-types";
import Search from "../../models/Search";
import ScreenComponent from "../../screens/_base/screen-component";


interface Props {
    keyword: string,
    helpKeyword: string,
    columnSelected: string,
    object: Search
    index: number
    onSearchCellClicked: (object: any) => void
}

export default class SearchResultListViewCell extends PureComponent<Props> {



    componentDidMount() {
        console.log("text ***", this.props.columnSelected);

    }

    renderTitle = (item) => {
        return (
            <Text numberOfLines={1} style={styles.mainTitleText}>
                {
                    // (this.props.columnSelected == 'title' || this.props.columnSelected == 'all') ? item.title!.trim().split(" ").map((item, index) => {
                    //     if (item.match(this.props.keyword)) {
                    //         return <Text style={{backgroundColor: '#FFDA44'}}> {item} </Text>
                    //     }
                    //     else if (item === this.props.helpKeyword) {
                    //         return <Text style={{backgroundColor: '#FD82F9'}}> {item} </Text>
                    //     }
                    //     else {
                    //         return " " + item + " "
                    //     }
                    // })
                    // :
                    item.title
                }
            </Text>
        )
    };
    renderDescription = (item) => {
        return (
            <Text numberOfLines={4} style={styles.desTitleText}>
                {
                    //     this.props.columnSelected == 'text' || this.props.columnSelected == 'all' ? item.clear_text!.trim().split(" ").map((item, index) => {
                    //     if (item.match(this.props.keyword)) {
                    //         return <Text style={{ backgroundColor: '#FFDA44' }}> {item} </Text>
                    //     }
                    //     else if (item === this.props.helpKeyword) {
                    //         return <Text style={{ backgroundColor: '#FD82F9' }}> {item} </Text>
                    //     }
                    //     else {
                    //         return " " + item + " "
                    //     }
                    // })
                    //     :
                    item.clear_text!
                }
            </Text>
        )
    };
    onLoadMoreClicked = (keyword: string, helpKeyword: string, column: string, categoryID: string) => {
        let object = { searchInputValue: keyword, helpWordInputValue: helpKeyword, column: column, category: categoryID };
        this.props.onSearchCellClicked(object)
    };


    render() {
        return (
            <Animatable.View useNativeDriver={true} animation={'fadeInUp'} style={styles.container}>
                <View style={styles.sectionTitleView}>
                    <Text style={styles.sectionTitle}>
                        نتائج البحث في قسم : {this.props.object.title} {"(" + this.props.object.cnt + ")"}
                    </Text>
                </View>
                {this.props.object.topRows!.map((item, index) => {
                    return (
                        <View style={styles.itemView}>
                            {this.renderTitle(item)}
                            {this.renderDescription(item)}
                        </View>
                    )
                })}
                <TouchableOpacity style={styles.loadMoreButton} onPress={
                    () => this.onLoadMoreClicked(this.props.keyword, this.props.helpKeyword, this.props.columnSelected, this.props.object.code!)
                }>
                    <Text style={styles.sectionTitle}>
                        شاهد المزيد
                    </Text>
                </TouchableOpacity>
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 5,
        paddingVertical: 2 * vh,
        paddingHorizontal: 1.9 * vw,
        backgroundColor: AppConstant.Color.Gray,
        marginBottom: 3 * vh,
        width: 91.4 * vw,
    },
    sectionTitleView: {
        alignItems: 'center',
        minHeight: 4.8 * vh,
        paddingHorizontal: 2 * vw,
        borderBottomWidth: 1,
        borderBottomColor: AppConstant.Color.White
    },
    loadMoreButton: {
        alignItems: 'center',
        minHeight: 4.8 * vh,
    },
    itemView: {
        flex: 1, borderRadius: 5, borderBottomWidth: 1, borderBottomColor: AppConstant.Color.White
    },
    sectionTitle: {
        textAlign: 'center',
        fontSize: 14,
        fontFamily: AppConstant.Font.SansArabicBold,
        color: AppConstant.Color.White,
    },
    mainTitleText: {
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 12,
        color: AppConstant.Color.White,
        textAlign: "center",
        marginTop: 1 * vh
    },
    desTitleText: {
        textAlign: "center",
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 2.8 * vw,
        color: AppConstant.Color.White,
        marginTop: 2 * vh
    }
});
