/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableOpacity } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import Input from "../../components/_base/input"
import AppConstant from "../../constant/Constant";
import ModalDropdown from 'react-native-modal-dropdown';
import { Icon } from "native-base";
import ValidationManager from "../../utils/validation-manager";

let Validator = require('validatorjs');


interface Props {
    onSendButtonClicked?: () => void
    validationManager?: ValidationManager
}

interface State {
    priority: string
    statues: string
}


export default class Form extends PureComponent<Props, State> {

    dropDownFatwaPriorityListRef;
    dropDownFatwaStatuesListRef;

    state = {
        priority: "سرعة الرد عادية",
        statues: "عام يمكن نشره"
    };

    onChooseFatwaPriorityButtonClicked = () => this.dropDownFatwaPriorityListRef.show();
    onChooseFatwaStatuesButtonClicked = () => this.dropDownFatwaStatuesListRef.show();

    componentWillMount() {
        Validator.register('equal', function (value, requirement, attribute) { // requirement parameter defaults to null
            return value == 11;
        }, 'العملية الحسابية خطأ');
    }

    onSelectPriority = (value: string, index: number) => {
        this.setState({
            priority: value,
        })
    };
    onSelectStatues = (value: string, index: number) => {
        this.setState({
            statues: value,
        })
    };

    render() {
        return (
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.inputView}>
                        <Text style={styles.inputTitle}>
                            الاسم
                        </Text>
                        <Input
                            itemStyle={styles.inputItem}
                            style={styles.input}
                            placeholder={'قم بادخال اسمك هنا'}
                            attributeName={'الاسم'}
                            constraints={'required'}
                            fieldName={'name'}
                            validationManager={this.props.validationManager}
                            errorMessageStyle={styles.errorMessageStyle}
                            placeholderTextColor={AppConstant.Color.GrayLightThree}
                            returnKeyType={'next'}
                        />
                    </View>
                    <View style={styles.inputView}>
                        <Text style={styles.inputTitle}>
                            البريد الالكتروني
                        </Text>
                        <Input
                            itemStyle={styles.inputItem}
                            style={styles.input}
                            attributeName={'الايميل'}
                            constraints={'required|email'}
                            fieldName={'email'}
                            keyboardType={'email-address'}
                            validationManager={this.props.validationManager}
                            errorMessageStyle={styles.errorMessageStyle}
                            placeholder={'قم بادخال بريدك الالكتروني هنا'}
                            placeholderTextColor={AppConstant.Color.GrayLightThree}
                            returnKeyType={'next'}
                        />
                    </View>
                    <View style={styles.inputView}>
                        <Text style={styles.inputTitle}>
                            تحديد اهمية الفتوى
                        </Text>
                        <TouchableOpacity style={styles.inputOptionView}
                            onPress={this.onChooseFatwaPriorityButtonClicked}>
                            <View style={styles.iconView}>
                                <Icon style={styles.icon} name={'md-arrow-dropdown'} />
                            </View>
                            <ModalDropdown
                                defaultValue={"سرعة الرد عادية"}
                                defaultIndex={0}
                                style={styles.modalDropDownView}
                                ref={(c) => this.dropDownFatwaPriorityListRef = c}
                                disabled={true}
                                textStyle={styles.modalDropDownText}
                                dropdownTextStyle={styles.dropDownText}
                                options={['سرعة الرد عادية', "سرعة الرد عالية", 'فتوى مستعجلة جدا']}
                                onSelect={(index, value) => this.onSelectPriority(value, index)}
                            />
                            <Input
                                update={true}
                                defaultValue={this.state.priority}
                                attributeName={'اهمية'}
                                constraints={'required'}
                                fieldName={'priority'}
                                validationManager={this.props.validationManager}
                                errorMessageStyle={styles.errorMessageStyle}
                                style={{ width: 0, height: 0, position: 'absolute' }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.inputView}>
                        <Text style={styles.inputTitle}>
                            حالة النشر في موقع النابلسي
                        </Text>
                        <TouchableOpacity style={styles.inputOptionView}
                            onPress={this.onChooseFatwaStatuesButtonClicked}>
                            <View style={styles.iconView}>
                                <Icon style={styles.icon} name={'md-arrow-dropdown'} />
                            </View>
                            <ModalDropdown
                                defaultValue={"عام يمكن نشره"}
                                defaultIndex={0}
                                style={styles.modalDropDownView}
                                ref={(c) => this.dropDownFatwaStatuesListRef = c}
                                disabled={true}
                                textStyle={styles.modalDropDownText}
                                dropdownTextStyle={styles.dropDownText}
                                onSelect={(index, value) => this.onSelectStatues(value, index)}
                                options={['عام يمكن نشره', "خاص غير قابل للنشر"]}
                            />
                            <Input
                                update={true}
                                attributeName={'حالة'}
                                constraints={'required'}
                                fieldName={'statues'}
                                validationManager={this.props.validationManager}
                                errorMessageStyle={styles.errorMessageStyle}
                                style={{ width: 0, height: 0, position: 'absolute' }}
                                defaultValue={this.state.statues}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.inputView}>
                        <Text style={styles.inputTitle}>
                            نص الفتوى
                        </Text>
                        <Input
                            itemStyle={styles.inputTextAreaItem}
                            style={styles.input}
                            placeholder={'قم بادخال بريدك الالكتروني هنا'}
                            attributeName={'نص الفتوى'}
                            constraints={'required'}
                            fieldName={'fatwa'}
                            validationManager={this.props.validationManager}
                            errorMessageStyle={styles.errorMessageStyle}
                            multiline={true}
                            placeholderTextColor={AppConstant.Color.GrayLightThree}
                            returnKeyType={'next'}
                        />
                    </View>
                    <View style={styles.inputView}>
                        <Text style={styles.inputTitle}>
                            الرجاء اتمم العملية الحسابية
                        </Text>
                        <View style={styles.calView}>
                            <View style={styles.sumView}>
                                <Text style={styles.sumText}> 8+3 </Text>
                            </View>
                            <View style={styles.equalView}>
                                <Text> = </Text>
                            </View>
                            <Input
                                itemStyle={[styles.inputItem, { flex: 1 }]}
                                style={styles.input}
                                placeholder={'قم بادخال الاجابة هنا'}
                                attributeName={'العملية الحسابية'}
                                constraints={'required|equal'}
                                fieldName={'calculate'}
                                validationManager={this.props.validationManager}
                                errorMessageStyle={styles.errorMessageStyle}
                                placeholderTextColor={AppConstant.Color.GrayLightThree}
                                keyboardType={'decimal-pad'}
                            />
                        </View>
                    </View>

                    <TouchableOpacity style={styles.buttonView} onPress={this.props.onSendButtonClicked}>
                        <Text style={styles.buttonText}>
                            إرسال الفتوى
                        </Text>
                    </TouchableOpacity>


                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 2.5 * vh,
        paddingHorizontal: 4.2 * vw,
    },
    inputItem: {
        width: '100%',
        marginTop: 1 * vh,
        borderWidth: 1,
        borderColor: AppConstant.Color.GrayMiddle,
        justifyContent: 'center',
        borderRadius: 5,
        backgroundColor: AppConstant.Color.White
    },
    inputOptionView: {
        width: '100%',
        height: 5.2 * vh,
        marginTop: 1 * vh,
        borderWidth: 1,
        borderColor: AppConstant.Color.GrayMiddle,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: AppConstant.Color.White
    },
    input: {
        flex: 1,
        height: 6 * vh,
        textAlignVertical: 'top',
        textAlign: 'right',
        marginRight: 8.8 * vw,
        color: AppConstant.Color.Black,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 1.7 * vh
    },
    inputTextAreaItem: {
        minHeight: 35.9 * vh,
        width: '100%',
        marginTop: 1 * vh,
        borderWidth: 1,
        borderColor: AppConstant.Color.GrayMiddle,
        borderRadius: 5,
        backgroundColor: AppConstant.Color.White
    },
    inputTitle: {
        textAlign: 'right', fontFamily: AppConstant.Font.SansArabic, fontSize: 18
    },
    inputView: {
        marginBottom: 3.2 * vh
    },
    modalDropDownView: {
        flex: 0.9, alignItems: 'flex-end', paddingRight: 8.8 * vw,
    },
    modalDropDownText: {
        color: AppConstant.Color.Black,
        fontFamily: AppConstant.Font.SansArabicBold,
        fontSize: 11,
        textAlign: 'right'
    },
    dropDownText: {
        color: AppConstant.Color.Black,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 11,
        textAlign: 'right'
    },
    iconView: {
        flex: 0.1,
        height: '100%',
        backgroundColor: AppConstant.Color.Yellow,
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        color: AppConstant.Color.White
    },
    calView: {
        flexDirection: 'row', justifyContent: 'center', alignItems: 'center',
    },
    sumView: {
        flex: 0.2, height: 5.2 * vh,
        marginTop: 1 * vh,
        borderWidth: 1,
        borderColor: AppConstant.Color.GrayMiddle,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: AppConstant.Color.White
    },
    sumText: {
        fontFamily: AppConstant.Font.SansArabicBold, fontSize: 20
    },
    equalView: {
        flex: 0.1, justifyContent: 'center', alignItems: 'center'
    },
    buttonView: {
        width: "100%",
        backgroundColor: AppConstant.Color.Yellow,
        height: 5.9 * vh,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonText: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabicBold,
        fontSize: 16
    },
    errorMessageStyle: {
        textAlign: 'right',
        fontSize: 10,
        marginRight: 8.8 * vw,
        fontFamily: AppConstant.Font.SansArabic,
        color: 'red',
    },
});
