/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, ViewStyle } from 'react-native';
import { BlurView } from 'react-native-blur';
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";


interface Props {
    onPressNewFatwaButtonClicked: () => void
    onPressFatwaButtonClicked: () => void
}


export default class FooterButton extends PureComponent<Props> {


    render() {
        return (
            <TouchableOpacity activeOpacity={1} style={styles.container}
                onPress={this.props.onPressNewFatwaButtonClicked}>
                <BlurView
                    style={styles.absolute as ViewStyle}
                    blurType="light"
                    blurAmount={10}
                />
                <Text style={styles.fatwaButtonTitle} onPress={this.props.onPressFatwaButtonClicked}>عرض قسم
                    الفتاوى</Text>
                <Text style={styles.title}>
                    إرسال فتوى جديدة
                </Text>
                <Text style={styles.fatwaButtonTitle} onPress={this.props.onPressFatwaButtonClicked}>عرض قسم
                    الفتاوى</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: 6.2 * vh,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    title: {
        fontFamily: AppConstant.Font.SansArabicBold,
        fontSize: 3.6 * vw,
        color: AppConstant.Color.Yellow,
    },
    fatwaButtonTitle: {
        fontFamily: AppConstant.Font.SansArabicBold,
        fontSize: 3 * vw,
        color: AppConstant.Color.GrayMiddle,
    },
    absolute: {
        backgroundColor: AppConstant.Color.White,
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
    },
});
