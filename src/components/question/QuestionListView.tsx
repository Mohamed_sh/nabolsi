/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, FlatList, RefreshControl } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import QuestionListViewCell from "./QuestionListViewCell";
import Question from "../../models/Question";
import SubCategory from "../../models/SubCategory";


interface Props {
    data: Array<SubCategory>
    onListViewCellClicked: (object: SubCategory) => void
    onPullToRefreshFired: () => void,
    onLoadMoreFired: () => void,
    renderEmptyComponent: JSX.Element,
    renderHeaderList: JSX.Element,
    renderFooterComponent: JSX.Element,
    pullToRefreshIndicatorVisible: boolean,
}


export default class QuestionListView extends PureComponent<Props> {

    _keyExtractor = (item, index) => index.toString();
    renderRefreshControl = () => (
        <RefreshControl
            refreshing={this.props.pullToRefreshIndicatorVisible}
            tintColor={AppConstant.Color.Black}
            onRefresh={this.props.onPullToRefreshFired}
        />
    );
    _renderItem = ({ item }) => (
        <QuestionListViewCell
            onListViewCellClicked={this.props.onListViewCellClicked}
            object={item}
        />
    );

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.props.data}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                    style={styles.list}
                    // onEndReachedThreshold={0.01}
                    onEndReached={this.props.onLoadMoreFired}
                    initialNumToRender={30}
                    ListHeaderComponent={this.props.renderHeaderList}
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={styles.contentContainerStyle}
                    refreshControl={this.renderRefreshControl()}
                    ListEmptyComponent={this.props.renderEmptyComponent}
                />
                {this.props.renderFooterComponent}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 2.4 * vh,
    },
    list: {},
    contentContainerStyle: {
        // alignItems: 'center',
        paddingBottom: 5 * vh
    },
    title: {
        fontFamily: AppConstant.Font.SansArabicBold,
        fontSize: 14,
        textAlign: 'right',
        marginVertical: 3.7 * vh
    },

});
