/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, TextStyle } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";


interface Props {
}


export default class Instructions extends PureComponent<Props> {


    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title as TextStyle}>الزوار الأعزاء</Text>
                <Text style={styles.description as TextStyle}>
                    أهلاً بكم في قسم الأسئلة والفتاوى، الخاص بمعالجة جميع أسئلة الزوار الكرام، يمكنكم أن ترسلوا أسئلتكم
                    وفتاواكم ليجيب عليها الدكتور محمد راتب النابلسي، نسأل الله لنا ولكم السداد. ويفضل إرسال الأسئلة
                    باللغة العربية إن أمكن لكننا أيضاً نستقبل الفتاوى والأسئلة باللغات كافة، ويرد عليها بحسب اللغة
                    المرسل بها
                    {"\n"}
                    {"\n"}
                    جميع الفتاوى تعامل معاملة واحدة وخاصة ولايتم تقديم فتوى على آخرى ولكن لضرورة الأحكام في بعض الحالات
                    الخاصة تم وضع خيار ( فتوى مستعجلة جداً ) يستخدم عند الضرورة القصوى فقط ،إن عملية نشر السؤال والإجابة
                    في قسم ( الفتاوى ) تتم بشكل يدوي، ونحن لاتقوم بنشر أي سؤال إلا بعد موافقة صاحب السؤال والتي يتم
                    سؤاله عنها في الفورم المخصص للأسئلة
                </Text>
                <Text style={styles.title as TextStyle}>الفتاوى التي تمت الاجابة عنها</Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 5 * vh,
        paddingHorizontal: 2.4 * vh,
    },
    title: {
        textAlign: 'right',
        fontFamily: AppConstant.Font.SansArabicBold,
        fontSize: 14,
        marginBottom: 3.5 * vh,

    },
    description: {
        textAlign: 'right',
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 12,
        marginBottom: 3.5 * vh,
    }
});
