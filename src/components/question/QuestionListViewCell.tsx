/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, TextStyle, TouchableOpacity } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import Question from "../../models/Question";
import moment from "moment";
import * as Animatable from 'react-native-animatable';
import SubCategory from "../../models/SubCategory";


interface Props {
    onListViewCellClicked: (object: SubCategory) => void
    object: SubCategory
}


export default class QuestionListViewCell extends PureComponent<Props> {


    render() {
        return (
            <Animatable.View useNativeDriver={true} animation={'fadeInUp'} style={styles.container}>
                <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.onListViewCellClicked(this.props.object)}>
                    <Grid>
                        <Col style={styles.leftView} size={1}>
                            <Text style={styles.dataTitle}>
                                تاريخ الفتوى
                            </Text>
                            <Text style={styles.title}>
                                {moment(this.props.object.article_date).format('DD/MM/YYYY')}
                            </Text>
                        </Col>
                        <Col style={styles.centerView} size={2}>
                            <Text numberOfLines={1} style={styles.title}>
                                الفتوى {this.props.object.id}
                            </Text>
                            <Text style={styles.subTitle}>
                                {this.props.object.short_title}
                            </Text>

                        </Col>
                        <Col style={styles.rightView} size={1}>
                            <Text style={styles.title}>
                                {this.props.object.category_name}
                            </Text>
                        </Col>
                    </Grid>
                </TouchableOpacity>
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: AppConstant.Color.Gray,
        borderRadius: 9,
        marginBottom: 3.7 * vh,
        width: 90.6 * vw,
        minHeight: 6.7 * vh,
        shadowColor: "#000",
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    },
    leftView: {
        justifyContent: 'center', alignItems: 'center'
    },
    rightView: {
        justifyContent: 'center', alignItems: 'center',
        padding: 3

    },
    centerView: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: AppConstant.Color.White,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingHorizontal: 3.4 * vw
    },
    dataTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 11,
    },
    title: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 3.7 * vw,
        textAlign: 'center'
    },
    subTitle: {
        textAlign: 'right',
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 10,
    }
});
