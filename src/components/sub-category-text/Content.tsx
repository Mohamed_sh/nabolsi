/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, ScrollView, Platform } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import SubCategory from "../../models/SubCategory";
import SubCategoryListViewCell from "../sub-category/SubCategoryListViewCell";
import Image from "../general/Image";
import MyWebView from 'react-native-webview-autoheight';


const htmlStyle = `<head>
                    <script>
                        document.addEventListener('DOMContentLoaded', function () {
                            document.title = document.getElementById('htmlContent').offsetHeight;
                            window.location.hash = 1;
                        });
                    </script>
                    <style>
                        body {
                          /*text-align: right;*/
                          background-color: #fff;
                          /*font-size: 5vw;*/
                          direction: rtl;
                          text-align: justify;
                          text-justify: inter-word;
                        }
                        /*p{*/
                            /*font-size: 4.5vw;*/
                            /*line-height: 5vh;*/
                            /*text-align: justify;*/
                            /*text-justify: inter-word;*/
                        /*}*/
                        .art_container{
                            margin-top: 20px;
                        }

                        .cat_cover{
                            height: 130px;
                            text-align: right;
                            line-height: 130px;
                            color: #fff;
                            font-size: 4em;
                            background-repeat: no-repeat;
                            padding-right: 3%;
                            background-size: cover;
                            border-radius: 5px 5px 0px 0px;
                            font-weight: bold;
                        }
                        .cat_tree ul{
                            list-style-type: none;
                            float: right;
                            width: 80vw;
                            background: #f5f5f5;
                            font-size: 1.2em;
                            padding-right: 10px;
                            font-weight: bold;
                            color: #000;
                            border-top: 1px solid;
                            border-bottom: 1px solid;
                        
                        }
                        .cat_tree ul li{
                            float: right;
                            padding: 5px 5px;
                        }
                        .cat_tree a:hover {
                            color: #6b85f5;
                        }
                        .cat_tree a {
                            color: #222;
                        }
                        .cat_tree ul li .fa{
                            padding-right: 5px;
                        }
                        .cat_tree li:last-child span.fa-arrow-left {
                            display: none;
                        }
                        .soundcloud_container{
                            width: 80%;
                            margin: 0 auto;
                            height: 150px;
                            overflow: hidden;
                        }
                        .art_body{
                            border: 1px solid #ccc;
                            border-top:0px;
                            padding: 0 20px;
                            margin-bottom: 40px;
                            padding-top: 60px;
                            font-size: 1.3em;
                            color: #000;
                            line-height: 2em;
                            border-radius: 0 0 10px 10px;
                            text-align: justify;
                        }
                        .art_header{
                            margin-top: 20px;
                            font-weight: bold;
                            color: #000;
                            font-size: 2em;
                            padding:-10px;
                        }
                        .bsmaleh{
                            text-align: center;
                            margin-bottom: 30px;
                            color: #444;
                        }
                        .header_text{
                            padding-top:20px;
                        }
                        .art_date{
                        
                            margin-top: -45px;
                        
                            text-align: left;
                        
                            color: #ccc;
                        
                            font-weight: initial;
                      
                        }
                        .art_footer{
                            width: 80vw;
                            text-align: center;
                            margin-bottom: 20px;
                        }
                        .eye_contianer{
                            margin: 0 auto;
                            width: 75px;
                            height: 25px;
                            background: #777;
                            color: #fff;
                            line-height: 25px;
                            border-radius: 3px;
                            font-size: 1.3em;
                        }
                        #sharedivcount{
                            max-width: 60px;
                            margin: 0 auto;
                            border: 1px #777 solid;
                            border-top: 0;
                            border-radius: 0 0 7px 8px;
                            color: #000;
                            height: 25px;
                            line-height: 25px;
                        }
                        .left_side_bar{
                            list-style-type: none;
                            width: 80vw;
                        }
                        .left_side_bar li{
                            border: 1px solid #ccc;
                            margin: 0 auto;
                            text-align: center;
                            border-radius: 3px;
                            padding: 20px 20px;
                            margin-bottom: 20px;
                        }
                        .language_item{
                            width: 35px;
                            height: 35px;
                            line-height: 28px;
                            /* float: left; */
                            padding: 3px;
                            /* font-size: 12px; */
                            font-weight: bold;
                            background: #cccccc;
                            margin: 2px;
                            border-radius: 5px;
                        }
                        .language_item a {
                            color: #fff;
                        }
                        
                        .language_item_container{
                            display: inline-block;
                        }
                        .documents_links .doc_item{
                            padding:5px;
                        }
                        .share_item{
                            margin-top: 10px;
                            margin-bottom: 10px;
                        }
                        .facebook_like_div{
                            width:50px;
                            margin-left: auto;
                            margin-right: auto;
                        }
                        
                        .title{
                            /*width: 80vw;*/
                            /*font-weight: bold;*/
                            /*font-size: 5vw;*/
                            /*line-height: 7vh;*/
                            text-align: right;
                            /*padding: 1vw 5vh;*/
                            /*margin: 10px 0;*/
                            color: #fff;
                            border-radius: 5px;
                            background: #2b7ba0;
                            background: -moz-linear-gradient(top, #2b7ba0 0%, #1f709b 50%, #2b7ba0 100%);
                            background: -webkit-linear-gradient(top, #2b7ba0 0%,#1f709b 50%,#2b7ba0 100%);
                            background: linear-gradient(to bottom, #2b7ba0 0%,#1f709b 50%,#2b7ba0 100%);
                            /*filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b7ba0', endColorstr='#2b7ba0',GradientType=0 );*/
                        }
                        .subtitle1{
                            /*width: 80vw;*/
                            /*font-weight: bold;*/
                            /*font-size: 3vw;*/
                            /*line-height: 7vh;*/
                            text-align: right;
                            /*padding: 1vw 5vh;*/
                            /*border-radius: 5px;*/
                            background: #2b7ba0;
                            background: -moz-linear-gradient(top, #2b7ba0 0%, #1f709b 50%, #2b7ba0 100%);
                            background: -webkit-linear-gradient(top, #2b7ba0 0%,#1f709b 50%,#2b7ba0 100%);
                            background: linear-gradient(to bottom, #2b7ba0 0%,#1f709b 50%,#2b7ba0 100%);
                            /*filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b7ba0', endColorstr='#2b7ba0',GradientType=0 );*/
                            color: #FFFFFF;
                            /*background: #51a1c7;*/
                          
                        
                        }
                        .subtitle2{
                         /*width: 80vw*/
                            /*font-weight: bold;*/
                            /*font-size: 3vw;*/
                            /*!*line-height: 7vh;*!*/
                            /*text-align: right;*/
                            /*!*padding: 1vw 5vh;*!*/
                            /*margin: 10px 0;*/
                            /*border-radius: 5px;*/
                            background: #2b7ba0;
                            background: -moz-linear-gradient(top, #2b7ba0 0%, #1f709b 50%, #2b7ba0 100%);
                            background: -webkit-linear-gradient(top, #2b7ba0 0%,#1f709b 50%,#2b7ba0 100%);
                            background: linear-gradient(to bottom, #2b7ba0 0%,#1f709b 50%,#2b7ba0 100%);
                            /*filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2b7ba0', endColorstr='#2b7ba0',GradientType=0 );*/
                            color: #FFFFFF;
                            /*padding-top: 15px;*/
                            /*padding-right: 30px;*/
                        }
                        .media_icon{
                            border: 0;
                        }
                        p.author {
                            text-align: left;
                            font-size: 0.6em;
                            color: #777;
                        }
                        p.quran {
                            text-align: center;
                            color: #0d2f61;
                            font-weight: bold;
                            margin: 0;
                        }
                        p.s7adeth {
                            text-align: center;
                            color: #004400;
                            font-weight: bold;
                            margin: 0;
                        }
                        .sh3er{
                            text-align: center;
                            font-weight: bold;
                        }
                        p {
                            margin-bottom: 5px;
                        }
                        p img{
                            width: 100%;
                        }
                        .art_body img{
                            width: 50%;
                            margin: 5px;
                            border: 5px #eee solid;
                        }
                        .i3lan img{
                            width: 50%;
                            border: 1px solid #ccc;
                            border-radius: 5%;
                            margin: 10px 0;
                        }

               </style>
               </head>`;


interface Props {
    isShowImage: boolean
    object: SubCategory
    onSubCategoryListViewCellClicked: (index: number, object: SubCategory) => void
}

export default class Content extends PureComponent<Props> {

    render() {
        return (
            <ScrollView scrollEnabled={true}
                showsVerticalScrollIndicator={false} style={styles.container}>
                <SubCategoryListViewCell isShortTitle={true} object={this.props.object}
                    onSubCategoryListViewCellClicked={this.props.onSubCategoryListViewCellClicked} />

                {this.props.isShowImage &&
                    <Image
                        style={styles.image}
                        resizeMode={'contain'}
                        source={{ uri: "http://alhudagroup-tr.com/web/uploads/articles_img/" + this.props.object.image }}
                    />
                }
                {/*<View style={{flex: 1, alignItems: 'center', padding: 1 * vh}}>*/}
                {/*<View style={this.props.isShowImage ? styles.textViewShort : styles.textView}>*/}
                <MyWebView
                    startInLoadingState={true}
                    originWhitelist={['*']}
                    useWebKit={true}
                    scalesPageToFit={true}
                    source={{
                        html: htmlStyle + (this.props.isShowImage ? this.props.object.text : this.props.object.text!.replace(/<img[^>]*>/g, "")),
                        baseUrl: 'http://alhudagroup-tr.com/web'
                    }}
                />
                {/*</View>*/}
                {/*</View>*/}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 2.5 * vh
    },
    image: {
        width: '100%',
        height: 25 * vh
    },
    cellView: {
        borderRadius: 9,
        width: 90.6 * vw,
        height: 6.7 * vh,
        flexDirection: 'row',
        shadowColor: "#000",
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: AppConstant.Color.Gray,
    },
    leftView: {
        justifyContent: 'center', alignItems: 'center'
    },
    rightView: {
        justifyContent: 'center', alignItems: 'center'

    },
    centerView: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: AppConstant.Color.White,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingHorizontal: 3.4 * vw
    },
    dataTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 11,
    },
    title: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 13,
    },
    subTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 10,
    },
    bookmarkView: {
        backgroundColor: AppConstant.Color.GrayLightTwo,
        justifyContent: 'center',
        alignItems: 'center',
        width: 6.9 * vw,
        height: 3.8 * vh,
        borderRadius: 3.8 * vh / 2,
        position: 'absolute',
        right: -6.9 * vw / 2,
        top: -3.8 * vh / 2
    },
    bookmarkImage: {
        width: 10.31,
        height: 12.19
    },
    textView: {
        width: 90 * vw,
        height: 65 * vh,
        borderRadius: 10,
        paddingVertical: 2.8 * vh,
        paddingHorizontal: 1 * vw,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        backgroundColor: AppConstant.Color.White,
        elevation: 5,
    },
    textViewShort: {
        marginTop: 2 * vh,
        width: 91.4 * vw,
        height: 53 * vh,
        borderRadius: 10,
        paddingVertical: 2.8 * vh,
        paddingHorizontal: 2.4 * vw,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        backgroundColor: AppConstant.Color.White,
        elevation: 5,
        flex: 1,
    },
    scrollView: {
        width: "100%"
    },
    scrollViewContent: {
        alignItems: 'flex-end'
    },
    button: {
        flex: 1,
        height: "100%",
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    buttonTitle: {
        textAlign: 'center',
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 14,
        color: AppConstant.Color.YellowDark,
        lineHeight: 17,
    },
    buttonIcon: {
        width: 20,
        height: 18
    },
    buttonView: {
        flex: 1,
        width: 90 * vw,
        backgroundColor: "#ffffff",
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: "center",
        borderBottomRightRadius: 9,
        borderBottomLeftRadius: 9,
        paddingVertical: 2 * vh,
        paddingTop: 3 * vh,
        zIndex: -1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,

    }
});
