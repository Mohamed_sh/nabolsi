/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, Image as RNImage, ImageStyle, TouchableOpacity, Slider, ScrollView } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import Video from "react-native-video";
import SubCategory from "../../models/SubCategory";
import moment from "moment";
import SubCategoryListViewCell from "../sub-category/SubCategoryListViewCell";
import Image from "../general/Image";


interface Props {
    object: SubCategory
    time: number,
    duration: number,
    soundImage: string,
    soundURL: string,
    paused: boolean,
    repeat: boolean,
    isEnd: boolean,
    setMediaPlayerRef: (mediaPlayerRef: Video) => void,
    setSliderOfTimeRef: (sliderOfTimeRef: Slider) => void,
    setPauseIconRef: (pauseIconRef: RNImage) => void,
    onMediaStartLoading: (data: any) => void,
    onMediaFinishLoading: (data: any) => void,
    onMediaPlayerProgress: (value: any) => void,
    onMediaFailedLoading: (error: any) => void,
    onNextButtonClicked: () => void,
    onMediaEnd: () => void,
    onReplaytButtonClicked: () => void,
    onPreviousButtonClicked: () => void,
    onTimeChange: (value: number) => void
    onPauseButtonClicked: () => void,
    onSubCategoryListViewCellClicked: (index: number, object: SubCategory) => void

}


export default class Content extends PureComponent<Props> {


    render() {
        return (
            <ScrollView scrollEnabled={false} showsVerticalScrollIndicator={false} style={styles.container}>
                <SubCategoryListViewCell isShortTitle={true} object={this.props.object}
                    onSubCategoryListViewCellClicked={this.props.onSubCategoryListViewCellClicked} />
                <View style={{ alignItems: 'center' }}>
                    <View style={styles.soundView}>
                        <Video
                            paused={this.props.paused}
                            audioOnly={true}
                            ref={(ref) => {
                                this.props.setMediaPlayerRef(ref)
                            }}
                            onError={(error) => this.props.onMediaFailedLoading(error)}
                            onLoadStart={(data) => this.props.onMediaStartLoading(data)}
                            onLoad={(data) => this.props.onMediaFinishLoading(data)}
                            volume={1}
                            repeat={false}
                            onEnd={() => this.props.onMediaEnd()}
                            source={{ uri: this.props.soundURL + '?client_id=95f22ed54a5c297b1c41f72d713623ef' }}
                            playInBackground={true}
                            ignoreSilentSwitch={"ignore"}
                            onProgress={(value) => this.props.onMediaPlayerProgress(value)}
                        />
                        {this.props.soundImage ?
                            <Image resizeMode={'contain'} style={styles.soundCoverImg as ImageStyle}
                                source={{ uri: this.props.soundImage }} />
                            :
                            <RNImage resizeMode={'contain'} style={styles.soundCoverImg as ImageStyle}
                                source={require("../../assets/images/sound_cover_img.png")} />
                        }
                        <Text style={styles.currentTimeText}>{AppConstant.convertMSToTime(this.props.time)}</Text>
                        <Slider
                            minimumTrackTintColor={AppConstant.Color.GrayMiddle}
                            thumbTintColor={AppConstant.Color.GrayMiddle}
                            style={styles.slider}
                            ref={(ref) => {
                                this.props.setSliderOfTimeRef(ref!)
                            }}
                            minimumValue={0}
                            maximumValue={this.props.duration}
                            onValueChange={(value) => this.props.onTimeChange(value)}
                        />
                        <Text style={styles.fullTimeText}>{AppConstant.convertMSToTime(this.props.duration)}</Text>
                        <View style={styles.controlButtonView}>
                            <TouchableOpacity onPress={this.props.onPreviousButtonClicked}>
                                <RNImage style={styles.prevImage as ImageStyle}
                                    source={require("../../assets/images/prev_img.png")} />
                            </TouchableOpacity>
                            {this.props.isEnd ?
                                <TouchableOpacity onPress={this.props.onReplaytButtonClicked}>
                                    <RNImage
                                        ref={(ref) => {
                                            this.props.setPauseIconRef(ref!)
                                        }}
                                        style={styles.pauseImage as ImageStyle}
                                        source={require("../../assets/images/repeat_ic.png")}
                                    />
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={this.props.onPauseButtonClicked}>
                                    <RNImage
                                        ref={(ref) => {
                                            this.props.setPauseIconRef(ref!)
                                        }}
                                        style={styles.pauseImage as ImageStyle}
                                        source={require("../../assets/images/pause_ic.png")}
                                    />
                                </TouchableOpacity>
                            }
                            <TouchableOpacity onPress={this.props.onNextButtonClicked}>
                                <RNImage style={styles.nextImage as ImageStyle}
                                    source={require("../../assets/images/next_ic.png")} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 3.7 * vh,
        // alignItems: 'center',
    },
    cellView: {
        borderRadius: 9,
        width: 90.6 * vw,
        height: 6.7 * vh,
        flexDirection: 'row',
        shadowColor: "#000",
        justifyContent: 'center',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: AppConstant.Color.Gray,
    },
    leftView: {
        justifyContent: 'center', alignItems: 'center'
    },
    rightView: {
        justifyContent: 'center', alignItems: 'center'

    },
    centerView: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: AppConstant.Color.White,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingHorizontal: 3.4 * vw
    },
    dataTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 11,
    },
    title: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 13,
    },
    subTitle: {
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        fontSize: 10,
    },
    textView: {
        width: 91.4 * vw,
        height: 72.8 * vh,
        backgroundColor: AppConstant.Color.White,
        borderRadius: 10,
        paddingVertical: 2.8 * vh,
        paddingHorizontal: 2.4 * vw,
        marginTop: 2.3 * vh,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    },
    scrollView: {
        width: "100%"
    },
    scrollViewContent: {
        alignItems: 'flex-end'
    },
    bookmarkView: {
        backgroundColor: AppConstant.Color.GrayLightTwo,
        justifyContent: 'center',
        alignItems: 'center',
        width: 6.9 * vw,
        height: 3.8 * vh,
        borderRadius: 3.8 * vh / 2,
        position: 'absolute',
        right: -6.9 * vw / 2,
        top: -3.8 * vh / 2
    },
    bookmarkImage: {
        width: 10.31,
        height: 12.19
    },
    soundView: {
        width: 91.4 * vw,
        height: 75.8 * vh,
        marginTop: 2 * vh,

    },
    soundCoverImg: {
        width: "100%", height: 30 * vh
    },
    currentTimeText: {
        color: AppConstant.Color.GrayMiddle, fontFamily: AppConstant.Font.SansArabic, fontSize: 16
    },
    fullTimeText: {
        textAlign: 'right', color: AppConstant.Color.GrayMiddle, fontFamily: AppConstant.Font.SansArabic, fontSize: 16
    },
    slider: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    controlButtonView: {
        flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', paddingHorizontal: 10 * vw
    },
    prevImage: {
        width: 47, height: 47
    },
    pauseImage: {
        width: 71.6, height: 71.6
    },
    nextImage: {
        width: 47, height: 47
    }
});
