/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { StyleSheet, FlatList, RefreshControl } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import NewListViewCell from "./NewListViewCell";
import AppConstant from "../../constant/Constant";
import New from "../../models/New";
import SubCategory from "../../models/SubCategory";
import NewStore from "../../strores/NewStore";


interface Props {
    data: Array<New>
    store: NewStore
    onListViewCellClicked: (object: SubCategory) => void
    onPullToRefreshFired: () => void,
    renderEmptyComponent: JSX.Element,
    pullToRefreshIndicatorVisible: boolean,
}


export default class NewListView extends PureComponent<Props> {

    _keyExtractor = (item, index) => index.toString();
    renderRefreshControl = () => (
        <RefreshControl
            refreshing={this.props.pullToRefreshIndicatorVisible}
            tintColor={AppConstant.Color.Black}
            onRefresh={this.props.onPullToRefreshFired}
        />
    );
    _renderItem = ({ item, index }) => {

        return (
            <NewListViewCell store={this.props.store} onListViewCellClicked={this.props.onListViewCellClicked} object={item} index={index} />
        )
    };

    render() {
        return (
            <FlatList
                data={this.props.data}
                renderItem={this._renderItem}
                keyExtractor={this._keyExtractor}
                style={styles.list}
                contentContainerStyle={styles.contentContainerStyle}
                refreshControl={this.renderRefreshControl()}
                ListEmptyComponent={this.props.renderEmptyComponent}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    list: {
        marginTop: 3.2 * vh
    },
    contentContainerStyle: {
        alignItems: 'center'
    }
});
