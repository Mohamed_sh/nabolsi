/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { PureComponent } from 'react';
import { View, StyleSheet, Image, ImageStyle, Text, TouchableOpacity, TextStyle } from 'react-native';
import { vw, vh } from "../../constant/UnitDim"
import AppConstant from "../../constant/Constant";
import New from "../../models/New";
import SubCategory from "../../models/SubCategory";
import NewStore from "../../strores/NewStore";
import VideoChannelScreen from "../../screens/video-channel/VideoChannelScreen";
import SoundChannelScreen from "../../screens/sound-channel/SoundChannelScreen";


interface Props {
    object: New
    index: number
    store: NewStore
    onListViewCellClicked: (object: SubCategory) => void

}


export default class NewListViewCell extends PureComponent<Props> {


    componentDidMount() {
        console.log("**************************************");

    }





    render() {
        console.log("data --->>>", this.props.store.subCategory![this.props.index]);
        console.log("data subCategory--->>>", this.props.store.subCategory![this.props.index]);


        return (
            <TouchableOpacity style={styles.container}
                onPress={() => {
                    if (this.props.object.hasOwnProperty('s_column') && this.props.object.s_column!.match('soundcloud')) {
                        SoundChannelScreen.push();
                    } else if (this.props.object.hasOwnProperty('s_column') && this.props.object.s_column!.match('youtube')) {
                        VideoChannelScreen.push()
                    } else {

                        const id = this.props.store.subCategory.filter(item => item.id === this.props.object.article_id)
                        this.props.onListViewCellClicked(id[0])
                    }
                }}>
                <View style={styles.dataView}>
                    <Text style={styles.dataTitle}>{this.props.object.dt_action_date}</Text>
                </View>
                <Text numberOfLines={1} style={this.props.index % 2 === 0 ? styles.titleWhite : styles.titleYellow}>
                    {this.props.object.hasOwnProperty('s_column') ? this.props.object.s_column : ""}
                </Text>
                <Text numberOfLines={1}
                    style={this.props.index % 2 === 0 ? styles.subTitleWhite : styles.subTitleYellow}>
                    {this.props.object.hasOwnProperty('title') ? this.props.object.title : ""}
                </Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: AppConstant.Color.Gray,
        borderRadius: 23,
        marginBottom: 3.7 * vh,
        width: 90.6 * vw,
        minHeight: 9 * vh,
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,

    },
    image: {
        width: 94,
        height: 90,
        marginLeft: 2 * vw,
        marginBottom: 1 * vh
    },
    dataView: {
        width: 23.2 * vw,
        height: '100%',
        backgroundColor: AppConstant.Color.White,
        position: 'absolute',
        borderRadius: 23,
        justifyContent: 'center',
        alignItems: 'center',
        right: 0,
    },
    dataTitle: {
        color: AppConstant.Color.GreenLight,
        fontSize: 14,
        fontFamily: AppConstant.Font.SansArabic
    },
    titleYellow: {
        textAlign: 'right',
        color: AppConstant.Color.Yellow,
        fontFamily: AppConstant.Font.SansArabicBold,
        marginRight: 26.6 * vw,
        paddingLeft: 5 * vw,
        fontSize: 14,
    },
    titleWhite: {
        textAlign: 'right',
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabicBold,
        marginRight: 26.6 * vw,
        paddingLeft: 5 * vw,
        fontSize: 14,
    },
    subTitleYellow: {
        textAlign: 'right',
        color: AppConstant.Color.Yellow,
        fontFamily: AppConstant.Font.SansArabic,
        marginRight: 26.6 * vw,
        paddingLeft: 5 * vw,
        fontSize: 12,
    },
    subTitleWhite: {
        textAlign: 'right',
        color: AppConstant.Color.White,
        fontFamily: AppConstant.Font.SansArabic,
        marginRight: 26.6 * vw,
        paddingLeft: 5 * vw,
        fontSize: 12,
    }
});
