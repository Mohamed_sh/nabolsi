import {Alert as RNAlert} from "react-native";

type ButtonStyleType = {
    default
    cancel
    destructive
}

class AlertUI {

    title: string;
    message: string;
    button: Array<object> = [];
    buttonStyle: ButtonStyleType;


    constructor(title: string, message: string) {
        this.title = title;
        this.message = message;
        this.buttonStyle = {
            default: 'default',
            cancel: 'cancel',
            destructive: 'destructive',
        }
    }

    public addAction = (text: string, style: string = this.buttonStyle.default, action?: Function) => {
        this.button.push({text: text, style: style, onPress: action})
    };

    public show = () => {
        RNAlert.alert(
            this.title,
            this.message,
            this.button,
        )
    }


}

export default AlertUI