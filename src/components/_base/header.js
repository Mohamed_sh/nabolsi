import React, {PureComponent} from "react";
import {Header, Left, Body, Right, Title, Button} from "native-base";
import PropTypes from 'prop-types';
import {View, StyleSheet} from "react-native";
import ScreenComponent from "../../screens/_base/screen-component";

export default class HeaderUI extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <Left>{this.props.left}</Left>
                <Body>
                <Title style={styles.title}>{this.props.title}</Title>
                </Body>
                <Right>
                    {this.props.right}
                </Right>
                {this.props.children}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title:{
        color: '#fff',
        fontFamily: ScreenComponent.isIOSPlatform() ? 'Gulf-SemiBold' : 'DINNextLTArabic-Regular',
        fontSize: ScreenComponent.isIOSPlatform() ? 22 : 23,
    }
});
HeaderUI.propTypes = {
    left: PropTypes.element,
    right: PropTypes.element,
    title: PropTypes.string,
    children: PropTypes.element,

};
