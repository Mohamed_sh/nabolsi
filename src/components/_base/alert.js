"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_native_1 = require("react-native");
var AlertUI = /** @class */ (function () {
    function AlertUI(title, message) {
        var _this = this;
        this.button = [];
        this.addAction = function (text, style, action) {
            if (style === void 0) { style = _this.buttonStyle.default; }
            _this.button.push({ text: text, style: style, onPress: action });
        };
        this.show = function () {
            react_native_1.Alert.alert(_this.title, _this.message, _this.button);
        };
        this.title = title;
        this.message = message;
        this.buttonStyle = {
            default: 'default',
            cancel: 'cancel',
            destructive: 'destructive',
        };
    }
    return AlertUI;
}());
exports.default = AlertUI;
