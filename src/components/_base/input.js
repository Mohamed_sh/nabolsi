import React, {PureComponent} from 'react';
import {
    View,
    StyleSheet, Image,
} from 'react-native';
import {Icon, Input, Item, ListItem, Text} from "native-base";


export default class InputUI extends PureComponent {

    inputValue;
    errorMessage;
    fieldName;
    attributeName;
    constraints;
    validationManager;

    setError = (errorMessage) => {
        this.errorMessage = errorMessage;
        this.forceUpdate(); // TODO: Check performance
    };

    constructor(props) {
        super(props);

        // TODO: check memory duplication
        this.fieldName = this.props.fieldName;
        this.constraints = this.props.constraints;
        this.attributeName = this.props.attributeName;
        this.validationManager = this.props.validationManager;
        this.inputValue = this.props.hasOwnProperty('defaultValue') ? this.props.defaultValue : null;

        if (this.validationManager) this.validationManager.addInput(this);
    }

    componentDidUpdate() {
        if (this.props.hasOwnProperty('defaultValue') && this.props.update) {
            this.inputValue = this.props.defaultValue;
        }
    }


    render() {
        return (
            <View style={this.props.itemStyle}>
                <Input
                    {...this.props}
                    underlineColorAndroid={'rgba(0,0,0,0)'}
                    onChangeText={(text) => this.inputValue = text}
                />
                {/*<Icon style={this.props.iconStyle} name={this.props.iconName}/>*/}

                {
                    this.errorMessage &&
                    <View style={styles.textError}>
                        <Text style={this.props.errorMessageStyle}>{this.errorMessage}</Text>
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textError: {
        borderBottomColor: 'rgba(0,0,0,0)',
        justifyContent: 'flex-end',
    },
});
