class New {
    pk_i_id?: number;
    dt_action_date?: string;
    s_column?: string;
    s_notification_txt?: string;
    title?: string;
    article_id?: number;
}

export default New