class Question {
    id?: number;
    category_id?: number;
    category_name?: string;
    category_name_ar?: string;
    category_description?: string;
    category_keywords?: string;
    category_the_order?: number;
    title?: string;
    short_title?: string;
    article_date?: string;
    inserted_date?: string;
    description?: string;
    keywords?: string;
    translator?: string;
    checker?: string;
    reviewer?: string;
    reference_arabic?: number;
    reference_english?: number;
    reference_french?: number;
    reference_spanish?: number;
    reference_chinese?: number;
    reference_persian?: number;
    reference_turkish?: number;
    video_library?: number;
    sound_library?: number;
    views?: number;
    in_home?: number;
    image?: string;
    reference_old_arabic?: number;
    attach?: (null)[] | null;
}

export default Question

export interface Weather {
    
}
