import SubCategory from "./SubCategory";

class Search {

    code?: string;
    title?: string;
    cnt?: string;
    topRows?: [SubCategory]
}

export default  Search