class SubCategory {

    id?: number;
    category_id?: number;
    category_name?: string;
    category_name_ar?: string;
    category_description?: string;
    category_keywords?: string;
    category_the_order?: number;
    title?: string;
    short_title?: string;
    text?: string;
    article_date?: string;
    inserted_date?: string;
    description?: string;
    keywords?: string;
    translator?: string;
    checker?: string;
    reviewer?: string;
    reference_arabic?: number;
    reference_english?: number;
    reference_french?: number;
    reference_spanish?: number;
    reference_chinese?: number;
    reference_persian?: number;
    reference_turkish?: number;
    video_library?: number;
    sound_library?: number;
    views?: number;
    in_home?: number;
    image?: string;
    clear_text?: string;
    reference_old_arabic?: number;
    attach?: (Attach)[] | null;

}


class  Attach {
    id?: string;
    article_id?: string;
    sn?: string;
    sr?: string;
    vw?: string;
    vd?: string;
    do?: string;
    pd?: string;
    yt?: string;
    sc?: string;
    sn_download?: string;
    sr_download?: string;
    vw_download?: string;
    vd_download?: string;
    do_download?: string;
    pd_download?: string;
}



export default SubCategory
