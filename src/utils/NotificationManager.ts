import firebase from "react-native-firebase";
import {Platform} from "react-native";


class NotificationManager {

    public static shared: NotificationManager = new NotificationManager();
    private FCMToken?: string;


    constructor() {
        this.creatingNotificationChannel();
        this.generateFCMToken();
        this.notificationPermissionRequest();
    }


    private generateFCMToken = () => {
        firebase.messaging().getToken()
            .then(token => this.FCMToken = token);
    };
    public getFCMToken = (): string => {
        return this.FCMToken!
    };

    private onNotificationReceived = (message: any) => {
        const notification = new firebase.notifications.Notification()
            .setTitle('نادي تشامبينز')
            .setBody(message.body)
            .android.setChannelId('Champions')
            .android.setLargeIcon('ic_launcher')
            .android.setDefaults([firebase.notifications.Android.Defaults.All])
            .android.setAutoCancel(true)
            .android.setPriority(firebase.notifications.Android.Priority.High);
        firebase.notifications().displayNotification(notification)
    };

    public addNotificationListener = () => {
        firebase.notifications().onNotification((message) => this.onNotificationReceived(message))
    };

    private notificationPermissionRequest = () => {
        firebase.messaging().requestPermission().then();
    };

    private creatingNotificationChannel = () => {
        const channel = new firebase.notifications.Android.Channel('Champions', 'نادي تشامبينز', firebase.notifications.Android.Importance.Max);
        firebase.notifications().android.createChannel(channel);
    };

    public getFCMType = (): number => {
        if (Platform.OS === 'ios') {
            return 2
        }
        else {
            return 1
        }
    };


}

export default NotificationManager