import {ApiResponse, ApiResponsePagination, RequestBuilder, RequestMethod} from "../api";
import {AxiosPromise} from "axios";

type paginationInformation = {
    i_current_page
    i_per_page
    i_total_pages
    i_total_objects
    i_items_on_page
}

export class ListViewManager {

    private pageNumber?: number;
    private pageCount?: number;
    private totalPage?: number;
    private itemOnPage?: number;
    private itemPerPage?: number;
    private totalItem?: number;
    private url?: string;
    private requestMethod?: string;
    private params = new Object();

    constructor() {
    }

    public firstPage = () => {
        this.setPageNumber(1);
        return this.requestBuilder()
    };
    public lastPage = () => {
        this.setPageNumber(-1);
        return this.requestBuilder()
    };
    public nextPage = () => {
        if (this.allowGoToNextPage()) {
            this.increasePageNumber();
            return this.requestBuilder()
        }
        else {
            return this.requestBuilder()
        }
    };
    public previousPage = () => {
        if (this.allowGoToPreviousPage()) {
            this.decreasePageNumber();
            return this.requestBuilder()
        }
    };

    public setURL(url: string) {
        this.url = url;
        return this;
    };

    public addParam(key: string, value: any = null) {
        this.params[key] = value;
        return this
    }

    public removeParam(key: string) {
        delete this.params[key];
        return this
    }

    public setPageNumber = (pageNumber: number) => {
        this.pageNumber = pageNumber;
        return this
    };

    public setRequestMethod = (requestMethod: string) => {
        this.requestMethod = requestMethod;
        return this
    };

    public get getPageNumber() {
        return this.pageNumber
    }

    public setPageCount = (pageCount: number) => {
        this.pageCount = pageCount;
        this.addParam('i_size', this.pageCount);
        return this
    };

    public get getPageCount() {
        return this.pageCount
    }

    public setTotalPage = (totalPage: number) => {
        this.totalPage = totalPage;
        return this
    };

    public get geTotalPage() {
        return this.totalPage
    }

    public setItemOnPage = (itemOnPage: number) => {
        this.itemOnPage = itemOnPage;
        return this
    };

    public get getItemOnPage() {
        return this.itemOnPage
    }

    public setItemPerPage = (itemPerPage: number) => {
        this.itemPerPage = itemPerPage;
        return this
    };

    public get getItemPerPage() {
        return this.itemPerPage
    }

    public setTotalItem = (totalItem: number) => {
        this.totalItem = totalItem;
        return this
    };

    public get getTotalItem() {
        return this.totalItem
    }

    public increasePageNumber = () => {
        this.setPageNumber(this.getPageNumber! + 1)
    };

    public decreasePageNumber = () => {
        this.setPageNumber(this.getPageNumber! - 1)

    };

    public setPaginationInformation = (paginationInformation: ApiResponsePagination) => {
        this.setPageNumber(paginationInformation.i_current_page!);
        this.setTotalPage(paginationInformation.i_total_pages!);
        this.setItemOnPage(paginationInformation.i_items_on_page!);
        this.setItemPerPage(paginationInformation.i_per_page!);
        this.setTotalItem(paginationInformation.i_total_objects!);
    };

    public allowGoToNextPage = () => {
        return this.getItemOnPage === this.getItemPerPage;
    };
    public allowGoToPreviousPage = () => {
        return this.getPageNumber! > 1;
    };

    public requestBuilder (): AxiosPromise<ApiResponse> {
        return new RequestBuilder()
            .setUrl(this.url!)
            .setMethod(this.requestMethod ? RequestMethod.POST : RequestMethod.GET)
            .setParam(this.params)
            .addParam('i_page', this.getPageNumber)
            .execute();
    }


}
