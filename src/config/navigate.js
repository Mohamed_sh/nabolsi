import { Navigation } from "react-native-navigation";
import Splash from "../../Splash";
import MainScreen from "../screens/main/MainScreen";
import main from "../screens/main/main";

const register = () => {
  Navigation.registerComponent("home", () => Splash);
  Navigation.registerComponent("MainScreen", () => MainScreen);
  Navigation.registerComponent("main", () => main);
};

export default register;
