// @ts-ignore
import {Navigation} from 'react-native-navigation';
import MainScreen from "../screens/main/MainScreen";
import CategoryScreen from "../screens/category/CategoryScreen";
import BookMarkScreen from "../screens/book-mark/BookMarkScreen";
import NewScreen from "../screens/new/NewScreen";
import SideMenuScreen from "../screens/side-menu/SideMenuScreen";
import TodayWisdomScreen from "../screens/today-wisdom/TodayWisdomScreen";
import WisdomArchiveScreen from "../screens/wisdom-archive/WisdomArchiveScreen";
import QuestionScreen from "../screens/question/QuestionScreen";
import SubCategoryScreen from "../screens/sub-category/SubCategoryScreen";
import SubCategorySoundScreen from "../screens/sub-category-sound/SubCategorySoundScreen";
import SubCategoryTextScreen from "../screens/sub-category-text/SubCategoryTextScreen";
import SubCategoryVideoScreen from "../screens/sub-category-video/SubCategoryVideoScreen";
import NewFatwaScreen from "../screens/new-fatwa/NewFatwaScreen";
import AdvancedSearchScreen from "../screens/advanced-search/AdvancedSearchScreen";
import SearchResultScreen from "../screens/search-result/SearchResultScreen";
import VideoChannelScreen from "../screens/video-channel/VideoChannelScreen";
import SoundChannelScreen from "../screens/sound-channel/SoundChannelScreen";
import SubCategoryPDFScreen from "../screens/sub-category-pdf/SubCategoryPDFScreen";
import ContactUsScreen from "../screens/contact-us/ContactUsScreen";
import SearchScreen from "../screens/search/SearchScreen";
import ServicesScreen from "../screens/services/ServicesScreen";
import SearchResultMoreScreen from "../screens/search-result-more/SearchResultMoreScreen";
import Splash from '../screens/main/Splash';

/**
 * import all app screens
 */



/**
 *  register app screens
 */
const registerScreens = () => {
    Navigation.registerComponent(Splash.screenID, () => Splash);
    Navigation.registerComponent(MainScreen.screenID, () => MainScreen);
    Navigation.registerComponent(CategoryScreen.screenID, () => CategoryScreen);
    Navigation.registerComponent(BookMarkScreen.screenID, () => BookMarkScreen);
    Navigation.registerComponent(NewScreen.screenID, () => NewScreen);
    Navigation.registerComponent(SideMenuScreen.screenID, () => SideMenuScreen);
    Navigation.registerComponent(TodayWisdomScreen.screenID, () => TodayWisdomScreen);
    Navigation.registerComponent(WisdomArchiveScreen.screenID, () => WisdomArchiveScreen);
    Navigation.registerComponent(QuestionScreen.screenID, () => QuestionScreen);
    Navigation.registerComponent(SubCategoryScreen.screenID, () => SubCategoryScreen);
    Navigation.registerComponent(SubCategorySoundScreen.screenID, () => SubCategorySoundScreen);
    Navigation.registerComponent(SubCategoryTextScreen.screenID, () => SubCategoryTextScreen);
    Navigation.registerComponent(SubCategoryVideoScreen.screenID, () => SubCategoryVideoScreen);
    Navigation.registerComponent(NewFatwaScreen.screenID, () => NewFatwaScreen);
    Navigation.registerComponent(AdvancedSearchScreen.screenID, () => AdvancedSearchScreen);
    Navigation.registerComponent(SearchResultScreen.screenID, () => SearchResultScreen);
    Navigation.registerComponent(VideoChannelScreen.screenID, () => VideoChannelScreen);
    Navigation.registerComponent(SoundChannelScreen.screenID, () => SoundChannelScreen);
    Navigation.registerComponent(SubCategoryPDFScreen.screenID, () => SubCategoryPDFScreen);
    Navigation.registerComponent(ContactUsScreen.screenID, () => ContactUsScreen);
    Navigation.registerComponent(SearchScreen.screenID, () => SearchScreen);
    Navigation.registerComponent(ServicesScreen.screenID, () => ServicesScreen);
    Navigation.registerComponent(SearchResultMoreScreen.screenID, () => SearchResultMoreScreen);
};

export default registerScreens

