/**
 * @format
 */

import { AppRegistry } from "react-native";
import { Navigation } from "react-native-navigation";
import App from "./App";
import { name as appName } from "./app.json";
import register from "./src/config/navigate";

register();

Navigation.startSingleScreenApp({
  screen: {
    screen: "home"
  }
});
// AppRegistry.registerComponent(appName, () => App);
